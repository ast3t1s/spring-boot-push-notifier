/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.util.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.notifier.boot.push.server.domain.entity.Device;
import com.notifier.boot.push.server.domain.entity.MessageBody;
import com.notifier.boot.push.server.domain.entity.PushMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class PushMessageSerializerTest {

    private final ObjectMapper objectMapper;

    private JacksonTester<PushMessage> jacksonTester;

    public PushMessageSerializerTest() {
        PusherModule pusherModule = new PusherModule();
        JavaTimeModule javaTimeModule = new JavaTimeModule();
        objectMapper = Jackson2ObjectMapperBuilder.json().modules(pusherModule, javaTimeModule).build();
    }

    @BeforeEach
    public void setUp() {
        JacksonTester.initFields(this, objectMapper);
    }

    @Test
    public void test_verify_serialize() throws IOException {
        List<Device> devices = new ArrayList<>();
        devices.add(Device.of("token1", "ios"));
        devices.add(Device.of("token2", "android"));

        Map<String, Object> extras = new LinkedHashMap<>();
        extras.put("data", "some-data");
        extras.put("field", "some value");
        Instant timestamp = Instant.now();

        MessageBody pushMessageBody = MessageBody.builder()
                .title("message title")
                .body("message body")
                .topic("test topic")
                .badge(4)
                .category("test - category")
                .sound("sound")
                .collapseKey("test-key")
                .icon("my icon")
                .validUntil(timestamp)
                .extras(extras)
                .build();

        PushMessage pushMessage = PushMessage.create(pushMessageBody, devices).build();

        JsonContent<PushMessage> jsonContent = jacksonTester.write(pushMessage);
        System.out.println(jsonContent.getJson());
        assertThat(jsonContent).extractingJsonPathValue("title").isEqualTo("message title");
        assertThat(jsonContent).extractingJsonPathValue("body").isEqualTo("message body");
        assertThat(jsonContent).extractingJsonPathNumberValue("badge").isEqualTo(4);
        assertThat(jsonContent).extractingJsonPathValue("category").isEqualTo("test - category");
        assertThat(jsonContent).extractingJsonPathValue("sound").isEqualTo("sound");
        assertThat(jsonContent).extractingJsonPathValue("icon").isEqualTo("my icon");
        assertThat(jsonContent).extractingJsonPathValue("valid_until").isEqualTo(timestamp.toString());
        assertThat(jsonContent).extractingJsonPathMapValue("extras").containsKeys("data", "field")
            .containsValues("some-data", "some value");
        assertThat(jsonContent).extractingJsonPathArrayValue("tokens").hasSize(2);
    }

    @Test
    public void test_verify_deserialize() throws IOException, JSONException {
        Map<String, Object> extras = new LinkedHashMap<>();
        extras.put("data", "some data");
        extras.put("field", "some value");

        JSONObject extrasJson = new JSONObject();
        for (Map.Entry<String, Object> entry : extras.entrySet()) {
            extrasJson.put(entry.getKey(), entry.getValue());
        }

        List<Device> devices = new ArrayList<>();
        devices.add(Device.of("token1", "ios"));
        devices.add(Device.of("token2", "android"));

        JSONArray tokensJson = new JSONArray();
        for (Device device : devices) {
            JSONObject deviceObj = new JSONObject();
            deviceObj.put("token", device.getToken());
            deviceObj.put("platform", device.getPlatform());
            tokensJson.put(deviceObj);
        }

        String json = new JSONObject().put("title", "message-title")
            .put("body", "message body")
            .put("badge", 4)
            .put("category", "test - category")
            .put("sound", "sound")
            .put("icon", "my icon")
            .put("collapse_key", "my key")
            .put("valid_until", Instant.now().toString())
            .put("extras", extrasJson)
            .put("tokens", tokensJson)
            .toString();

        System.out.println(json);
        PushMessage pushMessage = objectMapper.readValue(json, PushMessage.class);
        assertThat(pushMessage).isNotNull();
        assertThat(pushMessage.getMessageBody()).isNotNull();
        assertThat(pushMessage.getMessageBody().getTitle()).isEqualTo("message-title");
        assertThat(pushMessage.getMessageBody().getBody()).isEqualTo("message body");
        assertThat(pushMessage.getMessageBody().getSound()).isEqualTo("sound");
        assertThat(pushMessage.getMessageBody().getIcon()).isEqualTo("my icon");
        assertThat(pushMessage.getMessageBody().getCollapseKey()).isEqualTo("my key");
        assertThat(pushMessage.getMessageBody().getBadge()).isEqualTo(4);
        assertThat(pushMessage.getMessageBody().getExtras()).hasSize(2);
        assertThat(pushMessage.getMessageBody().getExtras()).containsKeys("data", "field");
        assertThat(pushMessage.getMessageBody().getExtras()).containsValues("some data", "some value");
        assertThat(pushMessage.getMessageBody().isSilent()).isFalse();
        assertThat(pushMessage.getTokens()).isEqualTo(devices);
    }
}
