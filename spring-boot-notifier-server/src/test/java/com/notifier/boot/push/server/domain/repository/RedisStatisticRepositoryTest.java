/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.domain.repository;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.redisson.Redisson;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import redis.embedded.RedisServer;

public class RedisStatisticRepositoryTest {

    public StatisticRepository repository;

    private final static RedisServer redisServer = new RedisServer();

    @BeforeAll
    public static void start() {
        redisServer.start();
    }

    @AfterAll
    public static void stop() {
        redisServer.stop();
    }

    @BeforeEach
    public void setUp() {
        this.repository = new RedisStatisticRepository(Redisson.createReactive());
    }

    @Test
    public void test_update_statistic() {
        StepVerifier.create(this.repository.addIosSuccess(4L)).expectNextCount(1).verifyComplete();
        StepVerifier.create(this.repository.addIosFailed(4L)).expectNextCount(1).verifyComplete();
        StepVerifier.create(this.repository.addAndroidSuccess(5L)).expectNextCount(1).verifyComplete();
        StepVerifier.create(this.repository.addAndroidFailed(5L)).expectNextCount(1).verifyComplete();
        StepVerifier.create(this.repository.addTotalCount(20L)).expectNextCount(1).verifyComplete();
    }

    @Test
    public void test_reset_statistic() {
        Mono.zip(this.repository.addIosSuccess(4L), this.repository.addIosFailed(4L), this.repository.addAndroidSuccess(5L),
                this.repository.addAndroidFailed(5L), this.repository.addTotalCount(20L))
                .as(StepVerifier::create)
                .expectNextCount(1)
                .verifyComplete();

        StepVerifier.create(this.repository.reset())
                .expectNextCount(0)
                .verifyComplete();
    }
}
