/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.util.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.notifier.boot.push.server.domain.entity.PushSendingResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class PushResultSerializerTest {

    private final ObjectMapper objectMapper;

    private JacksonTester<PushSendingResult> jacksonTester;

    public PushResultSerializerTest() {
        PusherModule pusherModule = new PusherModule();
        JavaTimeModule javaTimeModule = new JavaTimeModule();
        objectMapper = Jackson2ObjectMapperBuilder.json().modules(pusherModule, javaTimeModule).build();
    }

    @BeforeEach
    public void setUp() {
        JacksonTester.initFields(this, objectMapper);
    }

    @Test
    public void test_verify_serialize() throws IOException {
        PushSendingResult result = PushSendingResult.empty();

        JsonContent<PushSendingResult> jsonContent = jacksonTester.write(result);
        assertThat(jsonContent).extractingJsonPathNumberValue("$.ios.success").isEqualTo(0);
        assertThat(jsonContent).extractingJsonPathNumberValue("$.ios.failed").isEqualTo(0);
        assertThat(jsonContent).extractingJsonPathNumberValue("$.ios.pending").isEqualTo(0);
        assertThat(jsonContent).extractingJsonPathNumberValue("$.android.pending").isEqualTo(0);
        assertThat(jsonContent).extractingJsonPathNumberValue("$.android.success").isEqualTo(0);
        assertThat(jsonContent).extractingJsonPathNumberValue("$.android.failed").isEqualTo(0);
        assertThat(jsonContent).extractingJsonPathNumberValue("$.total").isEqualTo(0);
        assertThat(jsonContent).extractingJsonPathArrayValue("$.unregistered_tokens").hasSize(0);
    }
}
