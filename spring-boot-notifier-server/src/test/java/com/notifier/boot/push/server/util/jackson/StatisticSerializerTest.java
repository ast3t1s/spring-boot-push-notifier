/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.util.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.notifier.boot.push.server.domain.entity.ApplicationStatistic;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class StatisticSerializerTest {

    private final ObjectMapper objectMapper;

    private JacksonTester<ApplicationStatistic> jacksonTester;

    public StatisticSerializerTest() {
        PusherModule pusherModule = new PusherModule();
        JavaTimeModule javaTimeModule = new JavaTimeModule();
        objectMapper = Jackson2ObjectMapperBuilder.json().modules(pusherModule, javaTimeModule).build();
    }

    @BeforeEach
    public void init() {
        JacksonTester.initFields(this, objectMapper);
    }

    @Test
    public void test_verify_serialize() throws IOException {
        ApplicationStatistic statistic = ApplicationStatistic.builder()
                .total(100L).iosFailed(15L).iosSuccess(14L).androidFailed(34L).androidSuccess(31L)
                .build();

        JsonContent<ApplicationStatistic> jsonContent = jacksonTester.write(statistic);
        System.out.println(jsonContent.getJson());
        assertThat(jsonContent).extractingJsonPathNumberValue("$.ios.success").isEqualTo(14);
        assertThat(jsonContent).extractingJsonPathNumberValue("$.ios.failed").isEqualTo(15);
        assertThat(jsonContent).extractingJsonPathNumberValue("$.android.success").isEqualTo(31);
        assertThat(jsonContent).extractingJsonPathNumberValue("$.android.failed").isEqualTo(34);
        assertThat(jsonContent).extractingJsonPathNumberValue("$.total").isEqualTo(100);
    }

    @Test
    public void test_verify_deserialize() throws IOException, JSONException {
        JSONObject iosJson = new JSONObject()
                .put("success", 14)
                .put("failed", 34);

        JSONObject androidJson = new JSONObject()
                .put("success", 31)
                .put("failed", 34);

        String json = new JSONObject().put("ios", iosJson).put("android", androidJson).put("total", 100).toString();
        ApplicationStatistic statistic = objectMapper.readValue(json, ApplicationStatistic.class);
        assertThat(statistic).isNotNull();
        assertThat(statistic.getTotal()).isEqualTo(100);
        assertThat(statistic.getIosSuccess()).isEqualTo(14);
        assertThat(statistic.getIosFailed()).isEqualTo(34);
        assertThat(statistic.getAndroidSuccess()).isEqualTo(31);
        assertThat(statistic.getAndroidFailed()).isEqualTo(34);
    }
}
