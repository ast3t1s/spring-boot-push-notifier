/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.notifier;

import com.notifier.boot.push.server.domain.event.ApplicationEvent;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class CompositePushNotifierTest {

    private static final ApplicationEvent TEST_EVENT = new TestEvent();

    @Test
    public void invariants() {
        assertThatThrownBy(() -> new CompositePushNotifier(null)).isInstanceOf(IllegalArgumentException.class)
            .hasMessage("'delegates' must not be null");
    }

    @Test
    public void test_trigger_all_notifiers() {
        TestNotifier testHandler1 = new TestNotifier();
        TestNotifier testHandler2 = new TestNotifier();
        CompositePushNotifier compositeEventHandlerProcessor = new CompositePushNotifier(Arrays.asList(testHandler1, testHandler2));

        StepVerifier.create(compositeEventHandlerProcessor.notify(TEST_EVENT)).verifyComplete();

        assertThat(testHandler1.getEvents()).containsOnly(TEST_EVENT);
        assertThat(testHandler2.getEvents()).containsOnly(TEST_EVENT);
    }

    @Test
    public void test_continue_on_exception() {
        PushEventNotifier handler1 = (event) -> Mono.error(new IllegalStateException("Test"));
        TestNotifier testHandler2 = new TestNotifier();
        CompositePushNotifier compositeEventHandlerProcessor = new CompositePushNotifier(Arrays.asList(handler1, testHandler2));

        StepVerifier.create(compositeEventHandlerProcessor.notify(TEST_EVENT)).verifyComplete();

        assertThat(testHandler2.getEvents()).containsOnly(TEST_EVENT);
    }
}
