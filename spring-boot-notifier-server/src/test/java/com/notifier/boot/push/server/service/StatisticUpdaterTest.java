/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.service;

import com.notifier.boot.push.server.domain.repository.InMemoryStatisticRepository;
import com.notifier.boot.push.server.domain.repository.StatisticRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.test.StepVerifier;

public class StatisticUpdaterTest {

    private StatisticRepository statisticRepository;
    private StatisticUpdater statisticUpdater;

    @BeforeEach
    public void setUp() {
        this.statisticRepository = new InMemoryStatisticRepository();
        this.statisticUpdater = new StatisticUpdater(statisticRepository);
    }

    @Test
    public void test_update_application_statistic() {

        StepVerifier.create(statisticRepository.getTotalCount()).expectNext(0L).verifyComplete();
        StepVerifier.create(statisticRepository.getIosSuccess()).expectNext(0L).verifyComplete();
        StepVerifier.create(statisticRepository.getIosFailed()).expectNext(0L).verifyComplete();
        StepVerifier.create(statisticRepository.getAndroidSuccess()).expectNext(0L).verifyComplete();
        StepVerifier.create(statisticRepository.getAndroidFailed()).expectNext(0L).verifyComplete();

    }
}
