/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.service;

import com.notifier.boot.push.server.domain.entity.PushSendingResult;
import com.notifier.boot.push.server.domain.event.ApplicationEvent;
import com.notifier.boot.push.server.domain.event.SendDirectMessageEvent;
import com.notifier.boot.push.server.domain.event.UpdateStatisticEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.publisher.TestPublisher;

import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.clearInvocations;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;

public class StatisticUpdaterHandlerTest {

    private final PushSendingResult result = PushSendingResult.empty();

    private final TestPublisher<ApplicationEvent> events = TestPublisher.create();

    private StatisticUpdaterHandler trigger;

    private final StatisticUpdater mockUpdater = mock(StatisticUpdater.class);

    @BeforeEach
    public void setUp() {
        this.trigger = new StatisticUpdaterHandler(mockUpdater, events);
        this.trigger.start();
        await().until(this.events::wasSubscribed);
    }

    @Test
    public void test_should_not_update_when_stopped() {
        this.trigger.stop();
        clearInvocations(this.mockUpdater);
        this.events.next(new UpdateStatisticEvent(result));

        verify(this.mockUpdater, never()).updateAppStatistic(this.result);
    }

    @Test
    public void test_should_update_on_event() {
        this.events.next(new UpdateStatisticEvent(result));

        verify(this.mockUpdater, times(1)).updateAppStatistic(this.result);
    }

    @Test
    public void test_should_not_update_on_non_relevant_event() {
        this.events.next(new SendDirectMessageEvent(null, null));

        verify(this.mockUpdater, never()).updateAppStatistic(this.result);
    }

    @Test
    public void test_should_continue_update_after_error() throws InterruptedException {
        when(this.mockUpdater.updateAppStatistic(any())).thenReturn(Mono.error(IllegalStateException::new))
            .thenReturn(Mono.empty());

        this.events.next(new UpdateStatisticEvent(result));
        this.events.next(new UpdateStatisticEvent(result));

        verify(this.mockUpdater, times(2)).updateAppStatistic(this.result);
    }

}
