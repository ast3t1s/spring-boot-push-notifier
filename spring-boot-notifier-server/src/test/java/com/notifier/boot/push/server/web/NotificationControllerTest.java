/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.web;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.notifier.boot.push.server.domain.entity.Device;
import com.notifier.boot.push.server.domain.entity.MessageBody;
import com.notifier.boot.push.server.domain.entity.PushMessage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class NotificationControllerTest {

    private WebTestClient client;

    private ConfigurableApplicationContext instance;

    @BeforeEach
    public void setUp() {
        instance = new SpringApplicationBuilder().sources(NotificationTestApplication.class)
                .web(WebApplicationType.REACTIVE).run("--server.port=0");

        int localPort = instance.getEnvironment().getProperty("local.server.port", Integer.class, 0);

        this.client = WebTestClient.bindToServer().baseUrl("http://localhost:" + localPort).responseTimeout(Duration.ofMinutes(10)).build();
    }

    @AfterEach
    public void shutdown() {
        instance.close();
    }

    @Test
    public void test_send_push_notification() {

        MessageBody pushMessageBody = MessageBody.builder()
                .title("Title")
                .body("Test message")
                .build();

        List<Device> tokens = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            tokens.add(Device.of("dCJxROAk3Qw:APA91bFB2H4yHKBE5fCebgglLncas2Xt8SfRCcFyGXIlmvJ1GIluj1LkiPfvaO-1V8ISKivdNYp1gQF8hM9XzBwDFetSPER72pBoMhPMqd2fk3Sj-M-dUm7GAGfOO074UusEXDhInoX2", "android"));
            tokens.add(Device.of("Some invalid token", "android"));
        }

        PushMessage pushMessage = PushMessage.create(pushMessageBody, tokens).build();

        this.client.post().uri("/push/send")
                .body(BodyInserters.fromValue(pushMessage))
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody(String.class)
                .consumeWith((response) -> {
                    DocumentContext json = JsonPath.parse(response.getResponseBody());
                    System.out.println(json.jsonString());
                    assertThat(json.read("$.ios.pending", Long.class)).isEqualTo(0);
                    assertThat(json.read("$.ios.success", Long.class)).isEqualTo(0);
                    assertThat(json.read("$.ios.failed", Long.class)).isEqualTo(0);
                    assertThat(json.read("$.android.pending", Long.class)).isEqualTo(0);
                    assertThat(json.read("$.android.success", Long.class)).isEqualTo(10);
                    assertThat(json.read("$.android.failed", Long.class)).isEqualTo(10);
                    assertThat(json.read("$.total", Long.class)).isEqualTo(20);
                    assertThat(json.read("$.unregistered_tokens", List.class)).hasSize(0);
                });
    }

    @SpringBootApplication
    public static class NotificationTestApplication {

    }
}

