/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.web;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.notifier.boot.push.server.configuration.EnablePushNotificationServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.assertj.core.api.Assertions.assertThat;

public class ApplicationControllerIntTest {

    private WebTestClient client;

    private ConfigurableApplicationContext instance;

    @BeforeEach
    public void setUp() {
        instance = new SpringApplicationBuilder().sources(TestNotifierApplication.class)
                .web(WebApplicationType.REACTIVE).run("--server.port=0");

        int localPort = instance.getEnvironment().getProperty("local.server.port", Integer.class, 0);

        this.client = WebTestClient.bindToServer().baseUrl("http://localhost:" + localPort).build();
    }

    @AfterEach
    public void shutdown() {
        instance.close();
    }

    @Test
    public void test_get_application_statistic() {
        this.client.get().uri("/stat").exchange()
                .expectStatus()
                .isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(String.class)
                .consumeWith((response) -> {
                    DocumentContext json = JsonPath.parse(response.getResponseBody());
                    assertThat(json.read("$.ios.success", Long.class)).isEqualTo(0);
                    assertThat(json.read("$.ios.failed", Long.class)).isEqualTo(0);
                    assertThat(json.read("$.android.success", Long.class)).isEqualTo(0);
                    assertThat(json.read("$.android.failed", Long.class)).isEqualTo(0);
                    assertThat(json.read("$.total", Long.class)).isEqualTo(0);
                });
    }

    @Test
    public void test_reset_application_statistic() {
        this.client.post().uri("/stat/reset").exchange()
                .expectStatus()
                .isOk();
    }

    //@EnableWebSecurity
    @EnableWebFluxSecurity
    @SpringBootApplication
    @EnablePushNotificationServer
    public static class TestNotifierApplication {

        @Bean
        public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
            return http.authorizeExchange().anyExchange().permitAll().and().csrf().disable().build();
        }
    }
}
