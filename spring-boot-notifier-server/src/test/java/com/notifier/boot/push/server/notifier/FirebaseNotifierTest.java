/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.notifier;

import com.notifier.boot.push.server.domain.entity.Device;
import com.notifier.boot.push.server.domain.entity.MessageBody;
import com.notifier.boot.push.server.domain.entity.StatusInfo;
import com.notifier.boot.push.server.service.fcm.FirebaseClient;
import com.notifier.boot.push.server.service.fcm.model.FcmSuccessResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FirebaseNotifierTest {

    private FirebaseNotifier firebaseNotifier;

    private final FirebaseClient firebaseClient = mock(FirebaseClient.class);

    @BeforeEach
    public void setUp() {
        this.firebaseNotifier = new FirebaseNotifier(firebaseClient);
    }

    @Test
    public void test_send_success_push_message() {
        MessageBody pushMessageBody = MessageBody.builder().title("Title").body("Test message")
            .build();

        FcmSuccessResponse response = new FcmSuccessResponse();
        response.setName("projects/test-firebase-8b1ba/messages/0:1592230913215883%31bd1c9631bd1c96");

        when(this.firebaseClient.exchange(any(), anyBoolean())).thenReturn(Mono.just(response));

        StepVerifier.create(this.firebaseNotifier.sendMessageToAndroid(pushMessageBody, "some-token")).expectNext(StatusInfo.fromAndroid("some-token").ofSent(Device.ofAndroid("some-token"))).verifyComplete();
    }
}
