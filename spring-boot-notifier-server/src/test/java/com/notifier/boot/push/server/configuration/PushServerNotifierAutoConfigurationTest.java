/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.configuration;

import com.notifier.boot.push.server.notifier.CompositePushNotifier;
import com.notifier.boot.push.server.notifier.FirebaseNotifier;
import com.notifier.boot.push.server.notifier.PushEventNotifier;
import com.notifier.boot.push.server.notifier.TestNotifier;
import com.notifier.boot.push.server.service.PushNotificationSender;
import com.notifier.boot.push.server.service.fcm.FirebaseClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.test.context.runner.WebApplicationContextRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import static org.assertj.core.api.Assertions.assertThat;

public class PushServerNotifierAutoConfigurationTest {

    private final WebApplicationContextRunner contextRunner = new WebApplicationContextRunner()
        .withConfiguration(AutoConfigurations.of(WebMvcAutoConfiguration.class, PushServerAutoConfiguration.class, PushServerNotifierAutoConfiguration.class))
        .withUserConfiguration(PushServerMarkerConfiguration.class);

    @Test
    public void test_notifier_listeners() {
        this.contextRunner.withUserConfiguration(TestSingleNotifierConfig.class).run((context) -> {
            assertThat(context).getBean(PushEventNotifier.class).isInstanceOf(TestNotifier.class);
            assertThat(context).getBeans(PushEventNotifier.class).hasSize(1);
        });
    }

    @Test
    public void test_no_notifiers() {
        this.contextRunner.run((context) -> assertThat(context).doesNotHaveBean(PushNotificationSender.class));
    }

    @Test
    public void test_firebase_notifier() {
        this.contextRunner.withBean(FirebaseClient.class, () -> new FirebaseClient.Builder().credentials(new byte[0x01])
            .projectId("some-project")
            .firebaseMessageUrl("some-url").build()).withPropertyValues("spring.push.firebase.enabled:true").run((context) -> {
            assertThat(context).hasSingleBean(FirebaseNotifier.class);
        });
    }

    @Test
    public void test_apns_notifier() {
     /*   this.contextRunner.withPropertyValues("spring.push.apns.enabled:true").run((context) -> {
            assertThat(context).hasSingleBean(FirebaseNotifier.class);
        });*/
    }

    @Test
    public void test_multiple_notifiers() {
        this.contextRunner.withUserConfiguration(TestMultipleNotifierConfig.class).run((context) -> {
            assertThat(context.getBean(PushEventNotifier.class)).isInstanceOf(CompositePushNotifier.class);
            assertThat(context).getBeans(PushEventNotifier.class).hasSize(3);
        });
    }

    @Test
    public void test_multiple_notifiers_with_primary() {
        this.contextRunner.withUserConfiguration(TestMultipleWithPrimaryNotifierConfig.class).run((context) -> {
            assertThat(context.getBean(PushEventNotifier.class)).isInstanceOf(TestNotifier.class);
            assertThat(context).getBeans(PushEventNotifier.class).hasSize(2);
        });
    }

    public static class TestSingleNotifierConfig {

        @Bean
        @Qualifier("testNotifier")
        public TestNotifier testNotifier() {
            return new TestNotifier();
        }

    }

    public static class TestMultipleNotifierConfig {

        @Bean
        @Qualifier("testNotifier1")
        public TestNotifier testNotifier1() {
            return new TestNotifier();
        }

        @Bean
        @Qualifier("testNotifier2")
        public TestNotifier testNotifier2() {
            return new TestNotifier();
        }

    }

    public static class TestMultipleWithPrimaryNotifierConfig {

        @Bean
        @Primary
        @Qualifier("testNotifier")
        public TestNotifier testNotifierPrimary() {
            return new TestNotifier();
        }

        @Bean
        @Qualifier("testNotifier3")
        public TestNotifier testNotifier2() {
            return new TestNotifier();
        }

    }
}
