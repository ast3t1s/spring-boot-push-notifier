/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.configuration;

import com.notifier.boot.push.server.domain.repository.InMemoryStatisticRepository;
import com.notifier.boot.push.server.domain.repository.RedisStatisticRepository;
import com.notifier.boot.push.server.domain.repository.StatisticRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.redisson.Redisson;
import org.redisson.api.RedissonReactiveClient;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.test.context.runner.WebApplicationContextRunner;
import org.springframework.context.annotation.Bean;
import redis.embedded.RedisServer;

import static org.assertj.core.api.Assertions.assertThat;

public class PusherServerAutoConfigurationTest {

    private final WebApplicationContextRunner contextRunner = new WebApplicationContextRunner()
        .withConfiguration(AutoConfigurations.of(WebMvcAutoConfiguration.class, PushServerAutoConfiguration.class, PushServerRedisAutoConfiguration.class))
        .withUserConfiguration(PushServerMarkerConfiguration.class);

    private static final RedisServer redisServer = new RedisServer();

    @AfterAll
    public static void shutDown() {
        redisServer.stop();
    }

    @BeforeAll
    public static void setUp() {
        redisServer.start();
    }

    @Test
    public void test_base_config() {
        this.contextRunner.run(context -> assertThat(context).getBean(StatisticRepository.class).isInstanceOf(InMemoryStatisticRepository.class));
    }

    @Test
    public void test_redis_config() {
        this.contextRunner.withUserConfiguration(TestRedisConfig.class).withPropertyValues("spring.push.statistic.redis.enabled:true")
            .run((context) -> assertThat(context).getBean(StatisticRepository.class).isInstanceOf(RedisStatisticRepository.class));
    }

    public static class TestRedisConfig {

        @Bean
        public RedissonReactiveClient client() {
            return Redisson.createReactive();
        }
    }
}
