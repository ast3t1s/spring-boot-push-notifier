/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.domain.entity;

import com.notifier.boot.push.server.domain.entity.enumaration.Platform;
import com.notifier.boot.push.server.domain.entity.enumaration.Status;
import lombok.Data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class PushNotifyStatistic implements Serializable {

    private static final PushNotifyStatistic EMPTY = new PushNotifyStatistic(null, Collections.emptyList());

    private final Platform platform;

    private final Collection<StatusInfo> statusInfos;

    private PushNotifyStatistic(Platform platform, Collection<StatusInfo> statusInfos) {
        this.platform = platform;
        if (statusInfos.isEmpty()) {
            this.statusInfos = Collections.emptyList();
        } else {
            this.statusInfos = Collections.unmodifiableCollection(statusInfos);
        }
    }

    public static PushNotifyStatistic valueOf(Platform platform, Collection<StatusInfo> statusInfos) {
        if (platform == null || statusInfos == null) {
            return EMPTY;
        }
        return new PushNotifyStatistic(platform, statusInfos);
    }

    public static PushNotifyStatistic empty() {
        return EMPTY;
    }

    public List<Device> getUnregistered() {
        return getDevicesList().stream().filter(device -> device.getStatus() == Status.FAILED_DELETE)
                .filter(device -> device.getDevice().getDevicePlatform() == platform)
                .map(StatusInfo.DeviceStatus::getDevice)
                .collect(Collectors.toList());
    }

    public long getSent() {
        return count(Status.SENT);
    }

    public long getPending() {
        return count(Status.PENDING);
    }

    public long getFailed() {
        return count(Status.FAILED);
    }

    public long getTotal() {
        return getDevicesList().stream().filter(device -> device.getDevice().getDevicePlatform() == platform).count();
    }

    private Long count(Status status) {
        return getDevicesList().stream().filter(device -> device.getDevice().getDevicePlatform() == platform).filter(device -> device.getStatus() == status).count();
    }

    private List<StatusInfo.DeviceStatus> getDevicesList() {
        return statusInfos.stream().flatMap(StatusInfo::stream).collect(Collectors.toList());
    }

}
