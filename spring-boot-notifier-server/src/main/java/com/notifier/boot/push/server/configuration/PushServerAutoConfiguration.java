/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.configuration;

import com.notifier.boot.push.server.domain.event.ApplicationEvent;
import com.notifier.boot.push.server.domain.repository.InMemoryStatisticRepository;
import com.notifier.boot.push.server.domain.repository.StatisticRepository;
import com.notifier.boot.push.server.notifier.ReactiveEventPublisher;
import com.notifier.boot.push.server.service.StatisticUpdater;
import com.notifier.boot.push.server.service.StatisticUpdaterHandler;
import org.reactivestreams.Publisher;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.web.reactive.function.client.WebClientAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Lazy(false)
@Configuration(proxyBeanMethods = false)
@ConditionalOnBean(PushServerMarkerConfiguration.Marker.class)
@EnableConfigurationProperties(PushServerProperties.class)
@ImportAutoConfiguration({PushServerWebConfiguration.class})
@AutoConfigureAfter({WebClientAutoConfiguration.class})
public class PushServerAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(StatisticRepository.class)
    public InMemoryStatisticRepository inMemoryStatisticRepository() {
        return new InMemoryStatisticRepository();
    }

    @Bean
    @ConditionalOnMissingBean
    public StatisticUpdater statisticUpdater(StatisticRepository repository) {
        return new StatisticUpdater(repository);
    }

    @Bean(initMethod = "start", destroyMethod = "stop")
    @ConditionalOnMissingBean
    public StatisticUpdaterHandler statisticUpdaterTrigger(StatisticUpdater updater, Publisher<ApplicationEvent> publisher) {
        return new StatisticUpdaterHandler(updater, publisher);
    }

    @Bean
    @ConditionalOnMissingBean
    public ReactiveEventPublisher reactiveEventPublisher() {
        return new ReactiveEventPublisher();
    }
}
