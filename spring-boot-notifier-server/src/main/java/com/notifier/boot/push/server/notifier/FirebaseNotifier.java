/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.notifier;

import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.Message;
import com.notifier.boot.push.server.domain.entity.AndroidPayload;
import com.notifier.boot.push.server.domain.entity.Device;
import com.notifier.boot.push.server.domain.entity.MessageBody;
import com.notifier.boot.push.server.domain.entity.StatusInfo;
import com.notifier.boot.push.server.domain.entity.enumaration.Platform;
import com.notifier.boot.push.server.domain.event.SendDirectMessageEvent;
import com.notifier.boot.push.server.service.fcm.FcmModelConverter;
import com.notifier.boot.push.server.service.fcm.FirebaseClient;
import com.notifier.boot.push.server.service.fcm.exception.FirebaseMissingTokenException;
import com.notifier.boot.push.server.service.fcm.model.FcmErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class FirebaseNotifier extends AbstractPushNotificationNotifier {

    // FCM data only notification keys
    private static final String FCM_NOTIFICATION_KEY = "_notification";

    // Expected response String from FCM
    private static final String FCM_RESPONSE_VALID_REGEXP = "projects/.+/messages/.+";

    private final FirebaseClient client;

    public FirebaseNotifier(FirebaseClient client) {
        this.client = client;
    }

    @Override
    public Mono<StatusInfo> doPushNotify(SendDirectMessageEvent event) {
        return sendMessageToAndroid(event.getMessageBody(), event.getDevice().getToken());
    }

    public Mono<StatusInfo> sendMessageToAndroid(MessageBody body, String pushToken) {
        final Device device = Device.ofAndroid(pushToken);
        final StatusInfo result = StatusInfo.fromDevice(device);
        return client.exchange(buildAndroidMessage(body, pushToken), false)
            .filter(response -> response.getName() != null && response.getName().matches(FCM_RESPONSE_VALID_REGEXP))
            .map(response -> result.ofSent(device))
            .defaultIfEmpty(result.ofFailed(device))
            .onErrorResume((error) -> {
                log.error("Notification was rejected by FCM gateway", error);
                if (error instanceof FirebaseMissingTokenException) {
                    log.error("\t... due to device has bad auth token.");
                    return Mono.just(result.ofFailed(device));
                } else if (error instanceof WebClientResponseException) {
                    log.error("\t... due to device has some api error.");
                    final String errorCode = FcmModelConverter.convertExceptionToErrorCode((WebClientResponseException) error);
                    return Mono.just(parseErrorResponse(device, result, errorCode));
                } else {
                    log.error("\t... due to device has unknown error.");
                    return Mono.just(result.ofFailed(device));
                }
            });
    }

    private StatusInfo parseErrorResponse(Device device, StatusInfo result, String error) {
        switch (error) {
            case FcmErrorResponse.REGISTRATION_TOKEN_NOT_REGISTERED:
                log.error("Push message rejected by FCM gateway, device registration will be removed. Error: {}", error);
                return result.ofFailedDelete(device);
            case FcmErrorResponse.SERVER_UNAVAILABLE:
            case FcmErrorResponse.INTERNAL_ERROR:
            case FcmErrorResponse.MESSAGE_RATE_EXCEEDED:
                log.error("Push message rejected by FCM gateway, message status set to PENDING. Error: {}", error);
                return result.ofPending(device);
            case FcmErrorResponse.MISMATCHED_CREDENTIAL:
            case FcmErrorResponse.INVALID_APNS_CREDENTIALS:
            case FcmErrorResponse.INVALID_ARGUMENT:
            case FcmErrorResponse.UNKNOWN_ERROR:
                log.error("Push message rejected by FCM gateway, error: {}", error);
                return result.ofFailed(device);
            default:
                log.error("Unexpected error code received from FCM gateway: {}", error);
                return result.ofFailed(device);
        }
    }

    private Message buildAndroidMessage(final MessageBody body, final String pushToken) {
        Map<String, String> data = body.getExtras().entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().toString()));

        AndroidConfig.Builder androidConfigBuilder = AndroidConfig.builder()
            .setCollapseKey(body.getCollapseKey());

        if (body.getTimeToLive() != null) {
            androidConfigBuilder.setTtl(body.getTimeToLive());
        }

        AndroidNotification.Builder notificationBuilder = AndroidNotification.builder();
        notificationBuilder.setTitle(body.getTitle());
        notificationBuilder.setBody(body.getBody());
        notificationBuilder.setIcon(body.getIcon());
        notificationBuilder.setSound(body.getSound());
        notificationBuilder.setTag(body.getCategory());

        AndroidPayload androidPayload = body.getAndroidPayload();
        if (androidPayload != null) {
            notificationBuilder.setColor(androidPayload.getColor());
            notificationBuilder.setClickAction(androidPayload.getClickAction());
            notificationBuilder.setBodyLocalizationKey(androidPayload.getBodyLocKey());
            notificationBuilder.addAllBodyLocalizationArgs(androidPayload.getBodyLocArgs());
            notificationBuilder.setTitleLocalizationKey(androidPayload.getTitleLocKey());
            notificationBuilder.addAllTitleLocalizationArgs(androidPayload.getTitleLocArgs());
            if (!StringUtils.isEmpty(androidPayload.getTag())) {
                notificationBuilder.setTag(androidPayload.getTag());
            }
            if (!StringUtils.isEmpty(androidPayload.getIcon())) {
                notificationBuilder.setIcon(androidPayload.getIcon());
            }
        }

        if (!body.isSilent()) {
            androidConfigBuilder.setNotification(notificationBuilder.build());
        }

        Message.Builder messageBuilder = Message.builder();
        messageBuilder.setAndroidConfig(androidConfigBuilder.build());
        messageBuilder.putAllData(data);

        if (!StringUtils.isEmpty(body.getTopic())) {
            messageBuilder.setTopic(body.getTopic());
        } else {
            messageBuilder.setToken(pushToken);
        }

        return messageBuilder.build();
    }

    @Override
    public boolean shouldNotifyDevice(Platform platform) {
        return platform == Platform.ANDROID;
    }

}
