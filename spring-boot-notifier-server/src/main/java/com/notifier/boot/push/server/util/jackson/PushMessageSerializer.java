/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.util.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.notifier.boot.push.server.domain.entity.AndroidPayload;
import com.notifier.boot.push.server.domain.entity.Device;
import com.notifier.boot.push.server.domain.entity.IOSPayload;
import com.notifier.boot.push.server.domain.entity.PushMessage;
import org.springframework.util.StringUtils;

import java.io.IOException;

public class PushMessageSerializer extends StdSerializer<PushMessage> {

    public PushMessageSerializer() {
        super(PushMessage.class);
    }

    @Override
    public void serialize(PushMessage message, JsonGenerator generator, SerializerProvider serializerProvider) throws IOException {
        generator.writeStartObject();
        if (!StringUtils.isEmpty(message.getMessageBody().getTitle())) {
            generator.writeStringField("title", message.getMessageBody().getTitle());
        }
        if (!StringUtils.isEmpty(message.getMessageBody().getBody())) {
            generator.writeStringField("body", message.getMessageBody().getBody());
        }
        if (!StringUtils.isEmpty(message.getMessageBody().getTopic())) {
            generator.writeStringField("topic", message.getMessageBody().getTopic());
        }
        if (message.getMessageBody().getBadge() != null) {
            generator.writeNumberField("badge", message.getMessageBody().getBadge());
        }
        if (!StringUtils.isEmpty(message.getMessageBody().getCategory())) {
            generator.writeStringField("category", message.getMessageBody().getCategory());
        }
        if (!StringUtils.isEmpty(message.getMessageBody().getSound())) {
            generator.writeStringField("sound", message.getMessageBody().getSound());
        }
        if (!StringUtils.isEmpty(message.getMessageBody().getCollapseKey())) {
            generator.writeStringField("collapse_key", message.getMessageBody().getCollapseKey());
        }
        if (!StringUtils.isEmpty(message.getMessageBody().getIcon())) {
            generator.writeStringField("icon", message.getMessageBody().getIcon());
        }
        if (message.getMessageBody().getValidUntil() != null) {
            generator.writeStringField("valid_until", message.getMessageBody().getValidUntil().toString());
        }
        if (message.getMessageBody().isSilent()) {
            generator.writeBooleanField("silent", message.getMessageBody().isSilent());
        }
        if (message.getMessageBody().isMutableContent()) {
            generator.writeBooleanField("mutable_content", message.getMessageBody().isMutableContent());
        }
        if (message.getMessageBody().getAndroidPayload() != null) {
            AndroidPayload androidPayload = message.getMessageBody().getAndroidPayload();
            generator.writeObjectFieldStart("android_payload");
            if (!StringUtils.isEmpty(androidPayload.getIcon())) {
                generator.writeStringField("icon", androidPayload.getIcon());
            }
            if (!StringUtils.isEmpty(androidPayload.getTag())) {
                generator.writeStringField("tag", androidPayload.getTag());
            }
            if (!StringUtils.isEmpty(androidPayload.getColor())) {
                generator.writeStringField("color", androidPayload.getColor());
            }
            if (!StringUtils.isEmpty(androidPayload.getClickAction())) {
                generator.writeStringField("click_action", androidPayload.getClickAction());
            }
            if (!StringUtils.isEmpty(androidPayload.getBodyLocKey())) {
                generator.writeStringField("body_loc_key", androidPayload.getBodyLocKey());
            }
            if (!androidPayload.getBodyLocArgs().isEmpty()) {
                generator.writeFieldName("body_loc_args");
                generator.writeStartArray();
                for (String bodyLocArg : androidPayload.getBodyLocArgs()) {
                    generator.writeString(bodyLocArg);
                }
                generator.writeEndArray();
            }
            if (!StringUtils.isEmpty(androidPayload.getTitleLocKey())) {
                generator.writeStringField("title_loc_key", androidPayload.getTitleLocKey());
            }
            if (!androidPayload.getTitleLocArgs().isEmpty()) {
                generator.writeFieldName("title_loc_args");
                generator.writeStartArray();
                for (String titleLocArg : androidPayload.getTitleLocArgs()) {
                    generator.writeString(titleLocArg);
                }
                generator.writeEndArray();
            }
            generator.writeEndObject();
        }
        if (message.getMessageBody().getIosPayload() != null) {
            IOSPayload iosPayload = message.getMessageBody().getIosPayload();
            generator.writeObjectFieldStart("ios_alert");
            if (!StringUtils.isEmpty(iosPayload.getTitle())) {
                generator.writeStringField("title", iosPayload.getTitle());
            }
            if (!StringUtils.isEmpty(iosPayload.getBody())) {
                generator.writeStringField("body", iosPayload.getBody());
            }
            if (!StringUtils.isEmpty(iosPayload.getSubTitle())) {
                generator.writeStringField("subtitle", iosPayload.getSubTitle());
            }
            if (!StringUtils.isEmpty(iosPayload.getLaunchImage())) {
                generator.writeStringField("launch-image", iosPayload.getLaunchImage());
            }
            if (!StringUtils.isEmpty(iosPayload.getAction())) {
                generator.writeStringField("action", iosPayload.getAction());
            }
            if (!StringUtils.isEmpty(iosPayload.getActionLocKey())) {
                generator.writeStringField("action-loc-key", iosPayload.getActionLocKey());
            }
            if (!StringUtils.isEmpty(iosPayload.getLocKey())) {
                generator.writeStringField("loc-key", iosPayload.getLocKey());
            }
            if (iosPayload.getLocArgs().length > 0) {
                generator.writeFieldName("loc-args");
                generator.writeStartArray();
                for (String locArg : iosPayload.getLocArgs()) {
                    generator.writeString(locArg);
                }
                generator.writeEndArray();
            }
            if (!StringUtils.isEmpty(iosPayload.getTitleLocKey())) {
                generator.writeStringField("title-loc-key", iosPayload.getTitleLocKey());
            }
            if (iosPayload.getTitleLocArgs().length > 0) {
                generator.writeFieldName("title-loc-args");
                generator.writeStartArray();
                for (String locTitleArg : iosPayload.getTitleLocArgs()) {
                    generator.writeString(locTitleArg);
                }
                generator.writeEndArray();
            }
            if (!StringUtils.isEmpty(iosPayload.getLaunchImage())) {
                generator.writeStringField("launch-image", iosPayload.getLaunchImage());
            }
            generator.writeEndObject();
        }

        if (message.getMessageBody().getIosSound() != null) {
            generator.writeObjectFieldStart("ios_sound");
            if (!StringUtils.isEmpty(message.getMessageBody().getIosSound().getName())) {
                generator.writeStringField("name", message.getMessageBody().getIosSound().getName());
            }
            if (message.getMessageBody().getIosSound().getCritical() != null) {
                generator.writeBooleanField("critical", message.getMessageBody().getIosSound().getCritical());
            }
            if (message.getMessageBody().getIosSound().getVolume() != null) {
                generator.writeNumberField("volume", message.getMessageBody().getIosSound().getVolume());
            }
            generator.writeEndObject();
        }

        generator.writeObjectFieldStart("extras");
        for (final String key : message.getMessageBody().getExtras().keySet()) {
            generator.writeObjectField(key, message.getMessageBody().getExtras().get(key));
        }
        generator.writeEndObject();
        generator.writeFieldName("tokens");

        generator.writeStartArray();
        for (final Device token : message.getTokens()) {
            generator.writeStartObject();
            generator.writeStringField("token", token.getToken());
            generator.writeStringField("platform", token.getPlatform());
            generator.writeEndObject();
        }
        generator.writeEndArray();
        generator.writeEndObject();
    }
}
