/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.util.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.notifier.boot.push.server.domain.entity.Device;
import com.notifier.boot.push.server.domain.entity.IOSPayload;
import com.notifier.boot.push.server.domain.entity.IOSSound;
import com.notifier.boot.push.server.domain.entity.MessageBody;
import com.notifier.boot.push.server.domain.entity.PushMessage;
import com.notifier.boot.push.server.domain.entity.AndroidPayload;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class PushMessageDeserializer extends StdDeserializer<PushMessage> {

    public PushMessageDeserializer() {
        super(PushMessage.class);
    }

    @Override
    public PushMessage deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.readValueAsTree();
        MessageBody.Builder builder = MessageBody.builder();

        List<Device> tokens = new ArrayList<>();

        if (node.hasNonNull("title")) {
            builder.title(node.get("title").asText());
        }
        if (node.hasNonNull("body")) {
            builder.body(node.get("body").asText());
        }
        if (node.hasNonNull("topic")) {
            builder.topic(node.get("topic").asText());
        }
        if (node.hasNonNull("badge")) {
            builder.badge(node.get("badge").asInt());
        }
        if (node.hasNonNull("category")) {
            builder.category(node.get("category").asText());
        }
        if (node.hasNonNull("sound")) {
            builder.sound(node.get("sound").asText());
        }
        if (node.hasNonNull("collapse_key")) {
            builder.collapseKey(node.get("collapse_key").asText());
        }
        if (node.hasNonNull("icon")) {
            builder.icon(node.get("icon").asText());
        }
        if (node.hasNonNull("valid_until")) {
            builder.validUntil(Instant.parse(node.get("valid_until").asText()));
        }
        if (node.hasNonNull("silent")) {
            builder.silent(node.get("silent").asBoolean());
        }
        if (node.hasNonNull("mutable_content")) {
            builder.silent(node.get("mutable_content").asBoolean());
        }
        if (node.has("ios_alert")) {
            JsonNode iosAlertNode = node.get("ios_alert");
            IOSPayload.Builder iosPayloadBuilder = IOSPayload.builder();
            if (iosAlertNode.hasNonNull("title")) {
                iosPayloadBuilder.title(iosAlertNode.get("title").asText());
            }
            if (iosAlertNode.hasNonNull("body")) {
                iosPayloadBuilder.body(iosAlertNode.get("body").asText());
            }
            if (iosAlertNode.hasNonNull("subtitle")) {
                iosPayloadBuilder.subTitle(iosAlertNode.get("subtitle").asText());
            }
            if (iosAlertNode.hasNonNull("action")) {
                iosPayloadBuilder.action(iosAlertNode.get("action").asText());
            }
            if (iosAlertNode.hasNonNull("action-loc-key")) {
                iosPayloadBuilder.actionLocKey(iosAlertNode.get("action-loc-key").asText());
            }
            if (iosAlertNode.hasNonNull("launch-image")) {
                iosPayloadBuilder.launchImage(iosAlertNode.get("launch-image").asText());
            }
            if (iosAlertNode.hasNonNull("loc-args")) {
                if (iosAlertNode.get("loc-args").isArray()) {
                    for (JsonNode locArgsNode : iosAlertNode.get("loc-args")) {
                        iosPayloadBuilder.locArg(locArgsNode.asText());
                    }
                }
            }
            if (iosAlertNode.hasNonNull("loc-key")) {
                iosPayloadBuilder.locKey(iosAlertNode.get("loc-key").asText());
            }
            if (iosAlertNode.hasNonNull("title-loc-args")) {
                if (iosAlertNode.get("title-loc-args").isArray()) {
                    for (JsonNode titleLocArg : iosAlertNode.get("title-loc-args")) {
                        iosPayloadBuilder.titleLocArg(titleLocArg.asText());
                    }
                }
            }
            if (iosAlertNode.hasNonNull("title-loc-key")) {
                iosPayloadBuilder.titleLocKey(iosAlertNode.get("title-loc-key").asText());
            }
            builder.iosPayload(iosPayloadBuilder.build());
        }
        if (node.has("ios_sound")) {
            JsonNode iosSoundNode = node.get("ios_sound");
            IOSSound.IOSSoundBuilder iosSoundBuilder = IOSSound.builder();
            if (iosSoundNode.hasNonNull("name")) {
                iosSoundBuilder.name(iosSoundNode.get("name").asText());
            }
            if (iosSoundNode.hasNonNull("critical")) {
                iosSoundBuilder.critical(iosSoundNode.get("critical").asBoolean(false));
            }
            if (iosSoundNode.hasNonNull("volume")) {
                iosSoundBuilder.volume(iosSoundNode.get("volume").floatValue());
            }
            builder.iosSound(iosSoundBuilder.build());
        }
        if (node.has("android_payload")) {
            JsonNode androidPayloadNode = node.get("android_payload");
            AndroidPayload.Builder androidPayloadBuilder = AndroidPayload.builder();
            if (androidPayloadNode.hasNonNull("icon")) {
                androidPayloadBuilder.icon(androidPayloadNode.get("icon").asText());
            }
            if (androidPayloadNode.hasNonNull("tag")) {
                androidPayloadBuilder.tag(androidPayloadNode.get("tag").asText());
            }
            if (androidPayloadNode.hasNonNull("color")) {
                androidPayloadBuilder.color(androidPayloadNode.get("color").asText());
            }
            if (androidPayloadNode.hasNonNull("click_action")) {
                androidPayloadBuilder.clickAction(androidPayloadNode.get("click_action").asText());
            }
            if (androidPayloadNode.hasNonNull("body_loc_key")) {
                androidPayloadBuilder.bodyLocKey(androidPayloadNode.get("body_loc_key").asText());
            }
            if (androidPayloadNode.hasNonNull("body_loc_args")) {
                if (androidPayloadNode.get("body_loc_args").isArray()) {
                    for (JsonNode bodyLocArgs : androidPayloadNode.get("body_loc_args")) {
                        androidPayloadBuilder.bodyLocArg(bodyLocArgs.asText());
                    }
                }
            }
            if (androidPayloadNode.hasNonNull("title_loc_key")) {
                androidPayloadBuilder.titleLocKey(androidPayloadNode.get("title_loc_key").asText());
            }
            if (androidPayloadNode.hasNonNull("title_loc_args")) {
                if (androidPayloadNode.get("title_loc_args").isArray()) {
                    for (JsonNode titleLocArg : androidPayloadNode.get("title_loc_args")) {
                        androidPayloadBuilder.titleLocArg(titleLocArg.asText());
                    }
                }
            }
            builder.androidPayload(androidPayloadBuilder.build());
        }
        if (node.has("extras")) {
            Iterator<Map.Entry<String, JsonNode>> iterator = node.get("extras").fields();
            while (iterator.hasNext()) {
                Map.Entry<String, JsonNode> entry = iterator.next();
                builder.extras(entry.getKey(), entry.getValue().asText());
            }
        }
        if (node.hasNonNull("tokens")) {
            if (node.get("tokens").isArray()) {
                for (JsonNode tokenNode : node.get("tokens")) {
                    String token = tokenNode.get("token").asText();
                    String platform = tokenNode.get("platform").asText();
                    tokens.add(Device.of(token, platform));
                }
            }
        }

        return PushMessage.builder().messageBody(builder.build()).tokens(tokens).build();
    }
}
