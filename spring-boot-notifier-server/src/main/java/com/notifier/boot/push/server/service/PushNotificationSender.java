/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.service;

import com.notifier.boot.push.server.domain.entity.PushMessage;
import com.notifier.boot.push.server.domain.entity.PushSendingResult;
import com.notifier.boot.push.server.domain.event.SendDirectMessageEvent;
import com.notifier.boot.push.server.domain.event.UpdateStatisticEvent;
import com.notifier.boot.push.server.notifier.PushEventNotifier;
import com.notifier.boot.push.server.notifier.ReactiveEventPublisher;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.logging.Level;

@Slf4j
public class PushNotificationSender {

    private final PushEventNotifier notifier;

    private final ReactiveEventPublisher publisher;

    public PushNotificationSender(PushEventNotifier notifier, ReactiveEventPublisher publisher) {
        this.notifier = notifier;
        this.publisher = publisher;
    }

    public Mono<PushSendingResult> sendNotifications(PushMessage message) {
        return Flux.fromIterable(message.getTokens())
                .flatMapSequential((device) -> notifier.notify(new SendDirectMessageEvent(message.getMessageBody(), device))
                        .log(log.getName(), Level.FINEST))
                .subscribeOn(Schedulers.parallel())
                .onBackpressureBuffer()
                .collectList()
                .map(PushSendingResult::valueOf)
                .onErrorResume((error) -> Mono.just(PushSendingResult.empty()))
                .doOnSuccess(result -> publisher.publishEvent(new UpdateStatisticEvent(result)));
    }
}
