/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.notifier;

import com.notifier.boot.push.server.domain.entity.StatusInfo;
import com.notifier.boot.push.server.domain.entity.enumaration.Platform;
import com.notifier.boot.push.server.domain.event.ApplicationEvent;
import com.notifier.boot.push.server.domain.event.SendDirectMessageEvent;
import reactor.core.publisher.Mono;

public abstract class AbstractPushNotificationNotifier extends AbstractNotifier {

    @Override
    public boolean shouldNotify(ApplicationEvent event) {
        if (event instanceof SendDirectMessageEvent) {
            final SendDirectMessageEvent pushNotificationEvent = (SendDirectMessageEvent) event;
            final Platform devicePlatform = pushNotificationEvent.getDevice().getDevicePlatform();
            return shouldNotifyDevice(devicePlatform);
        }
        return false;
    }

    @Override
    public Mono<StatusInfo> doNotify(ApplicationEvent event) {
        if (event instanceof SendDirectMessageEvent) {
            return doPushNotify((SendDirectMessageEvent) event);
        }
        return Mono.empty();
    }

    public abstract Mono<StatusInfo> doPushNotify(SendDirectMessageEvent event);

    protected boolean shouldNotifyDevice(Platform platform) {
        return true;
    }
}
