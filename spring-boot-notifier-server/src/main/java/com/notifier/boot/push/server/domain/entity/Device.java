/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.domain.entity;

import com.notifier.boot.push.server.domain.entity.enumaration.Platform;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.util.Assert;

import java.io.Serializable;

@Data
@EqualsAndHashCode(of = {"token", "platform"})
public class Device implements Serializable {

    public static final String ANDROID = "android";

    public static final String IOS = "ios";

    private final String token;

    private final String platform;

    private Device(String token, String platform) {
        Assert.hasText(token, "'token' can not be empty");
        Assert.hasText(platform, "'platform' can not be empty");
        this.token = token;
        this.platform = platform.toLowerCase();
    }

    public static Device of(String token, String platform) {
        return new Device(token, platform);
    }

    public static Device ofAndroid(String token) {
        return of(token, ANDROID);
    }

    public static Device ofIos(String token) {
        return of(token, IOS);
    }

    public boolean isAndroid() {
        return ANDROID.equals(platform);
    }

    public boolean isIos() {
        return IOS.equals(platform);
    }

    public Platform getDevicePlatform() {
        return Platform.of(this.platform).orElse(Platform.UNKNOWN);
    }
}
