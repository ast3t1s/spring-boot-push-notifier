/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.service.fcm.model;

import com.google.api.client.util.Key;

import java.util.List;
import java.util.Map;

/**
 * Class containing response body from FCM server in case of error.
 *
 */
public class FcmErrorResponse {

    private static final String FCM_ERROR_TYPE = "type.googleapis.com/google.firebase.fcm.v1.FcmErrorCode";

    // FCM error codes, see class com.google.firebase.messaging.FirebaseMessaging for original definition of error codes
    public static final String INTERNAL_ERROR = "internal-error";
    public static final String UNKNOWN_ERROR = "unknown-error";
    public static final String REGISTRATION_TOKEN_NOT_REGISTERED = "registration-token-not-registered";
    public static final String INVALID_APNS_CREDENTIALS = "invalid-apns-credentials";
    public static final String INVALID_ARGUMENT = "invalid-argument";
    public static final String MESSAGE_RATE_EXCEEDED = "message-rate-exceeded";
    public static final String MISMATCHED_CREDENTIAL = "mismatched-credential";
    public static final String SERVER_UNAVAILABLE = "server-unavailable";

    @Key("error")
    private Map<String, Object> error;

    /**
     * Get FCM error code.
     * @return FCM error code.
     */
    public String getErrorCode() {
        if (error == null) {
            return null;
        }
        Object details = error.get("details");
        if (details instanceof List) {
            for (Object detail : (List) details) {
                if (detail instanceof Map) {
                    Map detailMap = (Map) detail;
                    if (FCM_ERROR_TYPE.equals(detailMap.get("@type"))) {
                        return (String) detailMap.get("errorCode");
                    }
                }
            }
        }
        return (String) error.get("status");
    }

    /**
     * Get FCM error message.
     *
     * @return FCM error message.
     */
    public String getErrorMessage() {
        return (String) error.get("message");
    }
}
