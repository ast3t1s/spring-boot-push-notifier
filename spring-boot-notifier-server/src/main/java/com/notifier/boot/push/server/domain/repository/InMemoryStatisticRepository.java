/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.domain.repository;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.util.concurrent.atomic.AtomicLong;

@Slf4j
public class InMemoryStatisticRepository implements StatisticRepository {

    private final AtomicLong totalCount = new AtomicLong(0);
    private final AtomicLong iosSuccessCount = new AtomicLong(0);
    private final AtomicLong iosFailedCount = new AtomicLong(0);
    private final AtomicLong androidFailedCount = new AtomicLong(0);
    private final AtomicLong androidSuccessCount = new AtomicLong(0);

    @Override
    public Mono<Long> addTotalCount(Long total) {
        log.debug("Add total count: {} to memory storage", total);
        return Mono.fromCallable(() -> totalCount.addAndGet(total));
    }

    @Override
    public Mono<Long> addIosSuccess(Long iosSuccess) {
        log.debug("Add ios success count: {} to memory storage", iosSuccess);
        return Mono.fromCallable(() -> iosSuccessCount.addAndGet(iosSuccess));
    }

    @Override
    public Mono<Long> addIosFailed(Long iosFailed) {
        log.debug("Add ios failed count: {} to memory storage", iosFailed);
        return Mono.fromCallable(() -> iosFailedCount.addAndGet(iosFailed));
    }

    @Override
    public Mono<Long> addAndroidSuccess(Long androidSuccess) {
        log.debug("Add android success count: {} to memory storage", androidSuccess);
        return Mono.fromCallable(() -> androidSuccessCount.addAndGet(androidSuccess));
    }

    @Override
    public Mono<Long> addAndroidFailed(Long androidFailed) {
        log.debug("Add android failed count: {} to memory storage", androidFailed);
        return Mono.fromCallable(() -> androidFailedCount.addAndGet(androidFailed));
    }

    @Override
    public Mono<Long> getTotalCount() {
        log.debug("Get total count from memory storage");
        return Mono.just(totalCount.get());
    }

    @Override
    public Mono<Long> getIosSuccess() {
        log.debug("Get ios success count from memory storage");
        return Mono.just(iosSuccessCount.get());
    }

    @Override
    public Mono<Long> getIosFailed() {
        log.debug("Get ios failed count from memory storage");
        return Mono.just(iosFailedCount.get());
    }

    @Override
    public Mono<Long> getAndroidSuccess() {
        log.debug("Get android success count from memory storage");
        return Mono.just(androidSuccessCount.get());
    }

    @Override
    public Mono<Long> getAndroidFailed() {
        log.debug("Get android failed count from memory storage");
        return Mono.just(androidFailedCount.get());
    }

    @Override
    public Mono<Void> reset() {
        log.debug("Reset in memory statistic storage");
        return Mono.fromRunnable(() -> {
            totalCount.set(0);
            iosSuccessCount.set(0);
            iosFailedCount.set(0);
            androidSuccessCount.set(0);
            androidFailedCount.set(0);
        });
    }
}
