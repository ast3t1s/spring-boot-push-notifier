/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.util.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.notifier.boot.push.server.domain.entity.ApplicationStatistic;

import java.io.IOException;

public class StatisticSerializer extends StdSerializer<ApplicationStatistic> {

    private static final long serialVersionUID = 1L;

    public StatisticSerializer() {
        super(ApplicationStatistic.class);
    }

    @Override
    public void serialize(ApplicationStatistic statistic, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeObjectFieldStart("ios");
        jsonGenerator.writeNumberField("success", statistic.getIosSuccess());
        jsonGenerator.writeNumberField("failed", statistic.getIosFailed());
        jsonGenerator.writeEndObject();
        jsonGenerator.writeObjectFieldStart("android");
        jsonGenerator.writeNumberField("success", statistic.getAndroidSuccess());
        jsonGenerator.writeNumberField("failed", statistic.getAndroidFailed());
        jsonGenerator.writeEndObject();
        jsonGenerator.writeNumberField("total", statistic.getTotal());
        jsonGenerator.writeEndObject();
    }
}
