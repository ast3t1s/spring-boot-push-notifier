/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.util.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.notifier.boot.push.server.domain.entity.ApplicationStatistic;

import java.io.IOException;

public class StatisticDeserializer extends StdDeserializer<ApplicationStatistic> {

    public StatisticDeserializer() {
        super(ApplicationStatistic.class);
    }

    @Override
    public ApplicationStatistic deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode node = jsonParser.readValueAsTree();
        ApplicationStatistic.Builder builder = ApplicationStatistic.builder();

        if (node.hasNonNull("ios")) {
            JsonNode iosNode = node.get("ios");
            if (iosNode.hasNonNull("success")) {
                builder.iosSuccess(iosNode.get("success").asLong());
            }
            if (iosNode.hasNonNull("failed")) {
                builder.iosFailed(iosNode.get("failed").asLong());
            }
        }
        if (node.hasNonNull("android")) {
            JsonNode androidNode = node.get("android");
            if (androidNode.hasNonNull("success")) {
                builder.androidSuccess(androidNode.get("success").asLong());
            }
            if (androidNode.hasNonNull("failed")) {
                builder.androidFailed(androidNode.get("failed").asLong());
            }
        }
        if (node.hasNonNull("total")) {
            builder.total(node.get("total").asLong());
        }

        return builder.build();
    }
}
