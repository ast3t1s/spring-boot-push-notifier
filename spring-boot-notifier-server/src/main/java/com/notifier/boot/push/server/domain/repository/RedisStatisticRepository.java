/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.domain.repository;

import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RAtomicLongReactive;
import org.redisson.api.RedissonReactiveClient;
import reactor.core.publisher.Mono;

@Slf4j
public class RedisStatisticRepository implements StatisticRepository {

    private static final String TOTAL_COUNT_KEY = "stat:total:count";
    private static final String IOS_SUCCESS_COUNT_KEY = "stat:ios:success:count";
    private static final String IOS_FAILED_COUNT_KEY = "stat:ios:failed:count";
    private static final String ANDROID_SUCCESS_COUNT_KEY = "stat:android:success:count";
    private static final String ANDROID_FAILED_COUNT_KEY = "stat:android:failed:count";

    private final RedissonReactiveClient client;

    private final RAtomicLongReactive totalCount;
    private final RAtomicLongReactive iosSuccessCount;
    private final RAtomicLongReactive iosFailedCount;
    private final RAtomicLongReactive androidSuccessCount;
    private final RAtomicLongReactive androidFailedCount;

    public RedisStatisticRepository(RedissonReactiveClient client) {
        this.client = client;
        this.totalCount = client.getAtomicLong(TOTAL_COUNT_KEY);
        this.iosSuccessCount = client.getAtomicLong(IOS_SUCCESS_COUNT_KEY);
        this.iosFailedCount = client.getAtomicLong(IOS_FAILED_COUNT_KEY);
        this.androidSuccessCount = client.getAtomicLong(ANDROID_SUCCESS_COUNT_KEY);
        this.androidFailedCount = client.getAtomicLong(ANDROID_FAILED_COUNT_KEY);
    }

    @Override
    public Mono<Long> addTotalCount(Long total) {
        log.debug("Add total messages count: {} to redis storage", total);
        return totalCount.addAndGet(total);
    }

    @Override
    public Mono<Long> addIosSuccess(Long iosSuccess) {
        log.debug("Add ios success count: {} to redis storage", iosSuccess);
        return iosSuccessCount.addAndGet(iosSuccess);
    }

    @Override
    public Mono<Long> addIosFailed(Long iosFailed) {
        log.debug("Add ios failed count: {} to redis storage", iosFailed);
        return iosFailedCount.addAndGet(iosFailed);
    }

    @Override
    public Mono<Long> addAndroidSuccess(Long androidSuccess) {
        log.debug("Add android success count: {} to redis storage", androidSuccess);
        return androidSuccessCount.addAndGet(androidSuccess);
    }

    @Override
    public Mono<Long> addAndroidFailed(Long androidFailed) {
        log.debug("Add android failed count: {} to redis storage", androidFailed);
        return androidFailedCount.addAndGet(androidFailed);
    }

    @Override
    public Mono<Long> getTotalCount() {
        log.debug("Get total count of sent messages from redis storage");
        return totalCount.get();
    }

    @Override
    public Mono<Long> getIosSuccess() {
        log.debug("Get count of ios success messages from redis storage");
        return iosSuccessCount.get();
    }

    @Override
    public Mono<Long> getIosFailed() {
        log.debug("Get count of ios failed messages from redis storage");
        return iosFailedCount.get();
    }

    @Override
    public Mono<Long> getAndroidSuccess() {
        log.debug("Get count of android success messages from redis storage");
        return androidSuccessCount.get();
    }

    @Override
    public Mono<Long> getAndroidFailed() {
        log.debug("Get count of android failed messages from redis storage");
        return androidFailedCount.get();
    }

    @Override
    public Mono<Void> reset() {
        log.debug("Reset redis statistic in redis storage");
        return Mono.zip(totalCount.set(0),
                iosSuccessCount.set(0),
                iosFailedCount.set(0),
                androidSuccessCount.set(0),
                androidFailedCount.set(0)).then();
    }
}
