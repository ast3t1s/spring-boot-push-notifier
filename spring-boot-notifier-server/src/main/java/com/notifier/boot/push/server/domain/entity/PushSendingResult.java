/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.domain.entity;

import com.notifier.boot.push.server.domain.entity.enumaration.Platform;
import com.notifier.boot.push.server.util.ListUtils;
import lombok.Data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Data
public class PushSendingResult implements Serializable {

    private static final PushSendingResult EMPTY = new PushSendingResult(Collections.emptyList());

    private final PushNotifyStatistic ios;

    private final PushNotifyStatistic android;

    private PushSendingResult(Collection<StatusInfo> statusInfos) {
        this.ios = PushNotifyStatistic.valueOf(Platform.IOS, statusInfos);
        this.android = PushNotifyStatistic.valueOf(Platform.ANDROID, statusInfos);
    }

    public List<Device> getUnregisterTokens() {
        return Collections.unmodifiableList(ListUtils.mergeLists(ios.getUnregistered(), android.getUnregistered()));
    }

    public static PushSendingResult valueOf(Collection<StatusInfo> statusInfos) {
        if (statusInfos == null || statusInfos.isEmpty()) {
            return empty();
        }
        return new PushSendingResult(statusInfos);
    }

    public static PushSendingResult empty() {
        return EMPTY;
    }

}
