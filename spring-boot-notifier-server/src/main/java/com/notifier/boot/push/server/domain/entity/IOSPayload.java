/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.domain.entity;

import lombok.Data;
import lombok.Singular;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public final class IOSPayload implements Serializable {

    private final String title;
    private final String body;
    private final String subTitle;
    private final String action;
    private final String actionLocKey;
    private final String launchImage;
    private final List<String> locArgs;
    private final String locKey;
    private final List<String> titleLocArgs;
    private final String titleLocKey;

    @lombok.Builder(builderClassName = "Builder", toBuilder = true)
    private IOSPayload(String title, String body, String subTitle, String action, String actionLocKey, String launchImage,
                       @Singular("locArg") List<String> locArgs, String locKey, @Singular("titleLocArg") List<String> titleLocArgs, String titleLocKey) {
        this.title = title;
        this.body = body;
        this.subTitle = subTitle;
        this.action = action;
        this.actionLocKey = actionLocKey;
        this.launchImage = launchImage;
        this.locArgs = new ArrayList<>(locArgs);
        this.locKey = locKey;
        this.titleLocArgs = new ArrayList<>(titleLocArgs);
        this.titleLocKey = titleLocKey;
    }

    public String[] getLocArgs() {
        String[] arrayLocArgs = new String[locArgs.size()];
        return locArgs.toArray(arrayLocArgs);
    }

    public String[] getTitleLocArgs() {
        String[] arrayArgs = new String[titleLocArgs.size()];
        return titleLocArgs.toArray(arrayArgs);
    }
}
