/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.service;

import com.notifier.boot.push.server.domain.event.ApplicationEvent;
import com.notifier.boot.push.server.domain.event.UpdateStatisticEvent;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
public class StatisticUpdaterHandler extends AbstractEventHandler<ApplicationEvent> {

    private final StatisticUpdater statisticUpdater;

    public StatisticUpdaterHandler(StatisticUpdater statisticUpdater, Publisher<ApplicationEvent> publisher) {
        super(publisher, ApplicationEvent.class);
        this.statisticUpdater = statisticUpdater;
    }

    @Override
    protected Publisher<Void> handleEvent(Flux<ApplicationEvent> publisher) {
        return publisher.filter((event) -> event instanceof UpdateStatisticEvent).flatMap(this::updateStatistic);
    }

    protected Mono<Void> updateStatistic(ApplicationEvent event) {
        UpdateStatisticEvent updateStatisticEvent = (UpdateStatisticEvent) event;
        return this.statisticUpdater.updateAppStatistic(updateStatisticEvent.getResult()).onErrorResume((cause) -> {
                log.warn("Unexpected error while updating statistic for {}", event, cause);
                return Mono.empty();
            });
    }
}
