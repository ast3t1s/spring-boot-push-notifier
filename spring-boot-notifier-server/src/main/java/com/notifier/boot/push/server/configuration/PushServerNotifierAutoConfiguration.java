/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.configuration;

import com.eatthepath.pushy.apns.ApnsClient;
import com.notifier.boot.push.server.notifier.PushEventNotifier;
import com.notifier.boot.push.server.notifier.ReactiveEventPublisher;
import com.notifier.boot.push.server.notifier.FirebaseNotifier;
import com.notifier.boot.push.server.notifier.ApnsNotifier;
import com.notifier.boot.push.server.notifier.CompositePushNotifier;
import com.notifier.boot.push.server.service.PushNotificationSender;
import com.notifier.boot.push.server.service.fcm.FirebaseClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.boot.autoconfigure.condition.NoneNestedConditions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.List;

@Configuration(proxyBeanMethods = false)
@AutoConfigureBefore(PushServerAutoConfiguration.class)
@AutoConfigureAfter(PushClientAutoConfiguration.class)
public class PushServerNotifierAutoConfiguration {

    @Configuration(proxyBeanMethods = false)
    @ConditionalOnBean(PushEventNotifier.class)
    public static class PushNotificationSenderConfiguration {

        @Bean
        @ConditionalOnMissingBean(PushNotificationSender.class)
        public PushNotificationSender cloudSender(PushEventNotifier notifier, ReactiveEventPublisher publisher) {
            return new PushNotificationSender(notifier, publisher);
        }
    }

    @Configuration(proxyBeanMethods = false)
    @ConditionalOnBean(PushEventNotifier.class)
    @AutoConfigureBefore({ PushNotificationSenderConfiguration.class })
    public static class CompositeNotifierConfiguration {

        @Bean
        @Primary
        @Conditional(NoSingleNotifierCandidateCondition.class)
        public CompositePushNotifier pushCloudNotifier(List<PushEventNotifier> pushNotifiers) {
            return new CompositePushNotifier(pushNotifiers);
        }

        static class NoSingleNotifierCandidateCondition extends NoneNestedConditions {

            NoSingleNotifierCandidateCondition() {
                super(ConfigurationPhase.REGISTER_BEAN);
            }

            @ConditionalOnSingleCandidate(PushEventNotifier.class)
            static class HasSingleNotifierInstance {

            }
        }
    }

    @Configuration(proxyBeanMethods = false)
    @ConditionalOnBean(ApnsClient.class)
    @AutoConfigureBefore({ PushNotificationSenderConfiguration.class, CompositeNotifierConfiguration.class})
    public static class ApnsNotifierConfiguration {

        @Value("${spring.push.apns.ios-bundle}")
        private final String iosBundle;

        public ApnsNotifierConfiguration(@Value("${spring.push.apns.ios-bundle}") String iosBundle) {
            this.iosBundle = iosBundle;
        }

        @Bean
        @ConditionalOnMissingBean
        public ApnsNotifier apnsNotifier(ApnsClient client) {
            ApnsNotifier apnsNotifier = new ApnsNotifier(client);
            apnsNotifier.setIosBundle(iosBundle);
            return apnsNotifier;
        }
    }

    @Configuration(proxyBeanMethods = false)
    @ConditionalOnBean(FirebaseClient.class)
    @AutoConfigureBefore({ PushNotificationSenderConfiguration.class, CompositeNotifierConfiguration.class})
    public static class FirebaseNotifierConfiguration {

        @Bean
        @ConditionalOnMissingBean
        public FirebaseNotifier firebaseNotifier(FirebaseClient firebaseClient) {
            return new FirebaseNotifier(firebaseClient);
        }
    }
}
