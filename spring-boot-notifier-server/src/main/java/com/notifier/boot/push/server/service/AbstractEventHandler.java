/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.service;

import com.notifier.boot.push.server.domain.event.ApplicationEvent;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import reactor.util.retry.Retry;

import javax.annotation.Nullable;
import java.util.logging.Level;

@Slf4j
public abstract class AbstractEventHandler<T extends ApplicationEvent> {

    private final Publisher<ApplicationEvent> publisher;
    private final Class<T> eventType;

    @Nullable
    private Disposable subscription;

    @Nullable
    private Scheduler scheduler;

    protected AbstractEventHandler(Publisher<ApplicationEvent> publisher, Class<T> eventType) {
        this.publisher = publisher;
        this.eventType = eventType;
    }

    public void start() {
        this.scheduler = Schedulers.boundedElastic();
        this.subscription = Flux.from(this.publisher)
                .subscribeOn(scheduler)
                .log(log.getName(), Level.FINEST)
                .doOnSubscribe(s -> log.debug("Subscribed to {} events", this.eventType))
                .ofType(this.eventType)
                .cast(this.eventType)
                .transform(this::handleEvent)
                .retryWhen(Retry.indefinitely().doBeforeRetry((s) -> log.warn("Unexpected error", s.failure())))
                .subscribe();
    }

    protected abstract Publisher<Void> handleEvent(Flux<T> publisher);

    public void stop() {
        if (this.subscription != null) {
            this.subscription.dispose();
            this.subscription = null;
        }
        if (this.scheduler != null) {
            this.scheduler.dispose();
            this.scheduler = null;
        }
    }
}
