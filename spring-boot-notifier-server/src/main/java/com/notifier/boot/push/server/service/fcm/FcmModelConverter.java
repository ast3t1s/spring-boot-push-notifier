/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.service.fcm;

import com.google.api.client.googleapis.util.Utils;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonGenerator;
import com.google.api.client.json.JsonParser;
import com.google.common.collect.ImmutableMap;
import com.google.firebase.messaging.Message;
import com.notifier.boot.push.server.service.fcm.model.FcmErrorResponse;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DefaultDataBuffer;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Flux;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Slf4j
@UtilityClass
public class FcmModelConverter {

    // See class com.google.firebase.messaging.FirebaseMessaging
    private final Map<String, String> FCM_ERROR_CODES = ImmutableMap.<String, String>builder()
            // FCM v1 canonical error codes
            .put("NOT_FOUND", FcmErrorResponse.REGISTRATION_TOKEN_NOT_REGISTERED)
            .put("PERMISSION_DENIED", FcmErrorResponse.MISMATCHED_CREDENTIAL)
            .put("RESOURCE_EXHAUSTED", FcmErrorResponse.MESSAGE_RATE_EXCEEDED)
            .put("UNAUTHENTICATED", FcmErrorResponse.INVALID_APNS_CREDENTIALS)

            // FCM v1 new error codes
            .put("APNS_AUTH_ERROR", FcmErrorResponse.INVALID_APNS_CREDENTIALS)
            .put("INTERNAL", FcmErrorResponse.INTERNAL_ERROR)
            .put("INVALID_ARGUMENT", FcmErrorResponse.INVALID_ARGUMENT)
            .put("QUOTA_EXCEEDED", FcmErrorResponse.MESSAGE_RATE_EXCEEDED)
            .put("SENDER_ID_MISMATCH", FcmErrorResponse.MISMATCHED_CREDENTIAL)
            .put("UNAVAILABLE", FcmErrorResponse.SERVER_UNAVAILABLE)
            .put("UNREGISTERED", FcmErrorResponse.REGISTRATION_TOKEN_NOT_REGISTERED)
            .build();

    // Google Json Factory (FCM model classes are not compatible with Jackson)
    private final JsonFactory JSON_FACTORY = Utils.getDefaultJsonFactory();

    /**
     * Converts WebClient Exception to FCM error code.
     *
     * @param exception WebClient response exception.
     * @return FCM error code.
     */
    public String convertExceptionToErrorCode(WebClientResponseException exception) {
        FcmErrorResponse response = new FcmErrorResponse();
        String code;
        try {
            String error = exception.getResponseBodyAsString();
            JsonParser parser = JSON_FACTORY.createJsonParser(error);
            parser.parseAndClose(response);
            code = FCM_ERROR_CODES.get(response.getErrorCode());
            if (code == null) {
                code = FcmErrorResponse.UNKNOWN_ERROR;
            }
        } catch (IOException ex) {
            log.error("Error occurred while parsing error response: {}", ex.getMessage(), ex);
            code = FcmErrorResponse.UNKNOWN_ERROR;
        }
        return code;
    }

    /**
     * Convert Message to payload for WebClient.
     *
     * @param message      Message to send.
     * @param validateOnly Whether to perform only validation.
     * @return Flux of DataBuffer.
     */
    public Flux<DataBuffer> convertMessageToFlux(Message message, boolean validateOnly) {
        ImmutableMap.Builder<String, Object> payloadBuilder = ImmutableMap.<String, Object>builder().put("message", message);
        if (validateOnly) {
            payloadBuilder.put("validate_only", true);
        }
        ImmutableMap<String, Object> payload = payloadBuilder.build();
        String convertedMessage;
        try {
            StringWriter writer = new StringWriter();
            JsonGenerator gen = JSON_FACTORY.createJsonGenerator(writer);
            gen.serialize(payload);
            gen.close();
            convertedMessage = writer.toString();
        } catch (IOException ex) {
            log.error("Json serialization failed: {}", ex.getMessage(), ex);
            return Flux.empty();
        }
        DefaultDataBufferFactory factory = new DefaultDataBufferFactory();
        DefaultDataBuffer dataBuffer = factory.wrap(ByteBuffer.wrap(convertedMessage.getBytes(StandardCharsets.UTF_8)));
        return Flux.just(dataBuffer);
    }

}
