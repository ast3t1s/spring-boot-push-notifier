/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.notifier;

import com.notifier.boot.push.server.domain.event.ApplicationEvent;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

import java.util.Collections;
import java.util.List;

@Slf4j
public class ReactiveEventPublisher implements Publisher<ApplicationEvent> {

    private final Flux<ApplicationEvent> publishFlux;
    private final Sinks.Many<ApplicationEvent> sink;

    public ReactiveEventPublisher() {
        this.sink = Sinks.many().unicast().onBackpressureBuffer();
        this.publishFlux = sink.asFlux().publish().autoConnect(0);
    }

    public void publishEvent(List<ApplicationEvent> events) {
        events.forEach((event) -> {
            log.debug("Event published {}", event);
            this.sink.tryEmitNext(event);
        });
    }

    public void publishEvent(ApplicationEvent event) {
        publishEvent(Collections.singletonList(event));
    }

    @Override
    public void subscribe(Subscriber<? super ApplicationEvent> subscriber) {
        publishFlux.subscribe(subscriber);
    }
}
