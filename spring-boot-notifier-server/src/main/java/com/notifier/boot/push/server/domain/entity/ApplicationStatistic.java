/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.domain.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.util.Assert;

import java.io.Serializable;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ApplicationStatistic implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Long total;

    private final Long iosSuccess;

    private final Long iosFailed;

    private final Long androidSuccess;

    private final Long androidFailed;

    @lombok.Builder(builderClassName = "Builder", toBuilder = true)
    private ApplicationStatistic(Long iosSuccess, Long iosFailed, Long androidSuccess, Long androidFailed, Long total) {
        Assert.isTrue(iosSuccess >= 0, "'iosSuccess' can not be negative");
        Assert.isTrue(iosFailed >= 0, "'iosFailed' can not be negative");
        Assert.isTrue(androidSuccess >= 0, "'androidSuccess' can not be negative");
        Assert.isTrue(androidFailed >= 0, "'androidFailed' can not be negative");
        Assert.isTrue(total >= 0, "'total' can not be negative");
        this.iosSuccess = iosSuccess;
        this.iosFailed = iosFailed;
        this.androidSuccess = androidSuccess;
        this.androidFailed = androidFailed;
        this.total = total;
    }
}
