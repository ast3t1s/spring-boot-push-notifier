/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.service;

import com.notifier.boot.push.server.domain.entity.PushSendingResult;
import com.notifier.boot.push.server.domain.repository.StatisticRepository;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
public class StatisticUpdater {

    private final StatisticRepository statisticRepository;

    public StatisticUpdater(StatisticRepository statisticRepository) {
        this.statisticRepository = statisticRepository;
    }

    public Mono<Void> updateAppStatistic(PushSendingResult result) {
        return Mono.zip(statisticRepository.addTotalCount(result.getAndroid().getTotal() + result.getIos().getTotal()),
            statisticRepository.addIosSuccess(result.getIos().getSent()),
            statisticRepository.addIosFailed(result.getIos().getFailed()),
            statisticRepository.addAndroidSuccess(result.getAndroid().getSent()),
            statisticRepository.addAndroidFailed(result.getAndroid().getFailed())
        ).then();
    }
}
