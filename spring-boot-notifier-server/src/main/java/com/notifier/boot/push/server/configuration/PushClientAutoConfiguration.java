/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.configuration;

import com.eatthepath.pushy.apns.ApnsClient;
import com.eatthepath.pushy.apns.ApnsClientBuilder;
import com.eatthepath.pushy.apns.auth.ApnsSigningKey;
import com.eatthepath.pushy.apns.proxy.HttpProxyHandlerFactory;
import com.notifier.boot.push.server.service.fcm.FirebaseClient;
import com.notifier.boot.push.server.service.fcm.FirebaseConfig;
import com.notifier.boot.push.server.util.CertificateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.net.ssl.SSLException;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Slf4j
@Configuration(proxyBeanMethods = false)
@AutoConfigureBefore({PushServerNotifierAutoConfiguration.class})
public class PushClientAutoConfiguration {

    @Configuration(proxyBeanMethods = false)
    @ConditionalOnProperty(prefix = "spring.push.firebase", value = "enabled", matchIfMissing = true)
    public static class FirebaseClientConfiguration {

        private final PushServerProperties properties;

        public FirebaseClientConfiguration(PushServerProperties properties) {
            this.properties = properties;
        }

        @Bean(initMethod = "start")
        @ConditionalOnMissingBean
        public FirebaseClient createFirebaseClient() {
            FirebaseClient.Builder builder = new FirebaseClient.Builder();
            if (properties.getFirebase().isProxyEnabled()) {
                FirebaseConfig.Proxy proxy = firebaseProxy();
                builder.proxy(proxy);
            }
            builder.firebaseMessageUrl(properties.getFirebase().getMessageUrl())
                    .projectId(properties.getFirebase().getProjectId())
                    .connectionTimeout((int) properties.getFirebase().getConnectionTimeout().toMillis())
                    .credentials(CertificateUtils.loadCertificate(properties.getFirebase().getCredentials()));
            return builder.build();
        }

        private FirebaseConfig.Proxy firebaseProxy() {
            return FirebaseConfig.Proxy.builder()
                    .host(properties.getFirebase().getProxy().getHost())
                    .port(properties.getFirebase().getProxy().getPort())
                    .username(properties.getFirebase().getProxy().getUsername())
                    .password(properties.getFirebase().getProxy().getPassword())
                    .build();
        }
    }

    @Configuration(proxyBeanMethods = false)
    @ConditionalOnProperty(prefix = "spring.push.apns", value = "enabled", matchIfMissing = true)
    public static class ApnsClientConfiguration {

        private final PushServerProperties properties;

        public ApnsClientConfiguration(PushServerProperties properties) {
            this.properties = properties;
        }

        @Bean(destroyMethod = "close")
        @ConditionalOnMissingBean
        public ApnsClient createApnsClient() {
            final ApnsClientBuilder apnsClientBuilder = new ApnsClientBuilder();
            apnsClientBuilder.setProxyHandlerFactory(apnsClientProxy());
            apnsClientBuilder.setConnectionTimeout(properties.getApns().getConnectionTimeout());
            if (properties.getApns().isDevelopment()) {
                log.info("Using APNS development host");
                apnsClientBuilder.setApnsServer(ApnsClientBuilder.DEVELOPMENT_APNS_HOST);
            } else {
                log.info("Using APNS production host");
                apnsClientBuilder.setApnsServer(ApnsClientBuilder.PRODUCTION_APNS_HOST);
            }
            try {
                ApnsSigningKey key = ApnsSigningKey.loadFromInputStream(CertificateUtils.loadCertificate(properties.getApns().getCertificate()), properties.getApns().getIosTeamId(), properties.getApns().getIosKeyId());
                apnsClientBuilder.setSigningKey(key);
            } catch (InvalidKeyException | NoSuchAlgorithmException | IOException e) {
                log.error(e.getMessage(), e);
            }
            try {
                return apnsClientBuilder.build();
            } catch (SSLException e) {
                log.error(e.getMessage(), e);
            }

            return null;
        }

        /**
         * Prepare proxy settings for APNs client.
         *
         * @return Proxy handler factory with correct configuration.
         */
        private HttpProxyHandlerFactory apnsClientProxy() {
            if (properties.getApns().isProxyEnable()) {
                String proxyUrl = properties.getApns().getProxy().getHost();
                int proxyPort = properties.getApns().getProxy().getPort();
                String proxyUsername = properties.getApns().getProxy().getUsername();
                String proxyPassword = properties.getApns().getProxy().getPassword();
                if (proxyUsername != null && proxyUsername.isEmpty()) {
                    proxyUsername = null;
                }
                if (proxyPassword != null && proxyPassword.isEmpty()) {
                    proxyPassword = null;
                }
                return new HttpProxyHandlerFactory(new InetSocketAddress(proxyUrl, proxyPort), proxyUsername, proxyPassword);
            }
            return null;
        }
    }
}
