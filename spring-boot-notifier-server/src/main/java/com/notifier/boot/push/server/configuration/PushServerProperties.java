/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.convert.DurationUnit;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

@Data
@ConfigurationProperties(prefix = "spring.push")
public class PushServerProperties {

    private final Apns apns = new Apns();

    private final Firebase firebase = new Firebase();

    private String path = "";

    public String apiPath(String apiPath) {
        return this.path + apiPath;
    }

    @Data
    public static class Apns {

        /**
         * On/Off for push notification to APNs.
         */
        private boolean enabled;

        /**
         * APNs key ID for token based provider.
         */
        private String iosKeyId;

        /**
         * APNs team ID for token based provider.
         */
        private String iosTeamId;

        /**
         * The assigned value of apns-topic for Request headers
         */
        private String iosBundle;

        /**
         * APNs certification file path.
         */
        private String certificate;

        /**
         * APNs proxy settings.
         */
        private final Proxy proxy = new Proxy();

        /**
         * Default timeout when making request to APNS service.
         */
        @DurationUnit(ChronoUnit.MILLIS)
        private Duration connectionTimeout = Duration.ofMillis(10_000);

        /**
         * On/Off for sandbox developments.
         */
        private boolean development;

        /**
         * On/Off APNs proxy.
         */
        private boolean proxyEnable;

    }

    @Data
    public static class Firebase {

        private final Proxy proxy = new Proxy();

        /**
         * On/Off for push notification to FCM.
         */
        private boolean enabled;

        /**
         * Firebase project ID.
         */
        private String projectId;

        /**
         * FCM google credentials path.
         */
        private String credentials;

        @DurationUnit(ChronoUnit.MILLIS)
        private Duration connectionTimeout = Duration.ofMillis(10_000);

        /**
         * Firebase message endpoint url.
         */
        private String messageUrl;

        /**
         * Set if proxy should be used for FCM communication.
         */
        private boolean proxyEnabled;

    }

    @Data
    public static class Proxy {
        /**
         * Proxy url.
         */
        private String host;
        /**
         * Proxy port.
         */
        private int port;
        /**
         * Proxy username.
         */
        private String username;
        /**
         * Proxy password.
         */
        private String password;
    }
}
