/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.notifier;

import com.eatthepath.pushy.apns.ApnsClient;
import com.eatthepath.pushy.apns.DeliveryPriority;
import com.eatthepath.pushy.apns.PushType;
import com.eatthepath.pushy.apns.util.ApnsPayloadBuilder;
import com.eatthepath.pushy.apns.util.SimpleApnsPayloadBuilder;
import com.eatthepath.pushy.apns.util.SimpleApnsPushNotification;
import com.eatthepath.pushy.apns.util.TokenUtil;
import com.notifier.boot.push.server.domain.entity.Device;
import com.notifier.boot.push.server.domain.entity.MessageBody;
import com.notifier.boot.push.server.domain.entity.StatusInfo;
import com.notifier.boot.push.server.domain.entity.IOSPayload;
import com.notifier.boot.push.server.domain.entity.IOSSound;
import com.notifier.boot.push.server.domain.entity.enumaration.Platform;
import com.notifier.boot.push.server.domain.event.SendDirectMessageEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;

import javax.annotation.Nullable;
import java.time.Instant;
import java.util.Map;

@Slf4j
public class ApnsNotifier extends AbstractPushNotificationNotifier {

    // APNS bad device token String
    private static final String APNS_BAD_DEVICE_TOKEN = "BadDeviceToken";

    // APNS device token not for topic String
    private static final String APNS_DEVICE_TOKEN_NOT_FOR_TOPIC = "DeviceTokenNotForTopic";

    // APNS topic disallowed String
    private static final String APNS_TOPIC_DISALLOWED = "TopicDisallowed";

    private final ApnsClient apnsClient;

    @Nullable
    private String iosBundle;

    public ApnsNotifier(ApnsClient apnsClient) {
        this.apnsClient = apnsClient;
    }

    @Override
    public Mono<StatusInfo> doPushNotify(SendDirectMessageEvent event) {
        if (iosBundle == null) {
            return Mono.error(new IllegalStateException("'iosBundle' must not be null."));
        }
        return sendPushToIosDevice(event.getMessageBody(), event.getDevice().getToken());
    }

    @Override
    public boolean shouldNotifyDevice(Platform platform) {
        return platform == Platform.IOS;
    }

    public Mono<StatusInfo> sendPushToIosDevice(MessageBody body, String pushToken) {
        final Device device = Device.ofIos(pushToken);
        final StatusInfo info = StatusInfo.fromDevice(device);
        final String token = TokenUtil.sanitizeTokenString(pushToken);
        final String payload = buildApnsPayload(body);
        final Instant validUntil = body.getValidUntil();
        final PushType pushType =  body.isSilent() ? PushType.BACKGROUND : PushType.ALERT; // iOS 13 and higher requires apns-push-type value to be set
        final SimpleApnsPushNotification pushNotification = new SimpleApnsPushNotification(token, iosBundle, payload, validUntil, DeliveryPriority.IMMEDIATE, pushType, body.getCollapseKey());

        return Mono.fromFuture(apnsClient.sendNotification(pushNotification)).map(response -> {
            if (!response.isAccepted()) {
                log.error("Notification rejected by the APNs gateway: {}", response.getRejectionReason());
                if (response.getRejectionReason().equals(APNS_BAD_DEVICE_TOKEN)) {
                    log.error("\t... due to bad device token value.");
                    return info.ofFailedDelete(device);
                } else if (response.getRejectionReason().equals(APNS_DEVICE_TOKEN_NOT_FOR_TOPIC)) {
                    log.error("\t... due to device token not for topic error.");
                    return info.ofFailedDelete(device);
                } else if (response.getRejectionReason().equals(APNS_TOPIC_DISALLOWED)) {
                    log.error("\t... due to topic disallowed error.");
                    return info.ofFailedDelete(device);
                } else if (response.getTokenInvalidationTimestamp().isPresent()) {
                    log.error("\t... and the token is invalid as of " + response.getTokenInvalidationTimestamp().get());
                    return info.ofFailedDelete(device);
                } else {
                    return info.ofFailed(device);
                }
            } else {
                log.info("Notification sent, APNS ID: {}", response.getApnsId());
                return info.ofSent(device);
            }
        }).onErrorResume((error) -> Mono.just(info.ofFailed(device))).defaultIfEmpty(info.ofPending(device));
    }

    /**
     * Method to build APNs message payload.
     *
     * @param messageBody Push message object with APNs data.
     * @return String with APNs JSON payload.
     */
    private String buildApnsPayload(MessageBody messageBody) {
        final ApnsPayloadBuilder payloadBuilder = new SimpleApnsPayloadBuilder();
        payloadBuilder.setAlertTitle(messageBody.getTitle());
        payloadBuilder.setAlertBody(messageBody.getBody());
        payloadBuilder.setBadgeNumber(messageBody.getBadge());
        payloadBuilder.setCategoryName(messageBody.getCategory());
        payloadBuilder.setSound(messageBody.getSound());
        payloadBuilder.setContentAvailable(messageBody.isSilent());
        payloadBuilder.setThreadId(messageBody.getCollapseKey());
        payloadBuilder.setMutableContent(messageBody.isMutableContent());
        if (messageBody.getIosPayload() != null) {
            final IOSPayload iosPayload = messageBody.getIosPayload();
            if (!StringUtils.isEmpty(iosPayload.getTitle())) {
                payloadBuilder.setAlertTitle(iosPayload.getTitle());
            }
            if (!StringUtils.isEmpty(iosPayload.getBody())) {
                payloadBuilder.setAlertBody(iosPayload.getBody());
            }
            payloadBuilder.setAlertSubtitle(iosPayload.getSubTitle());
            payloadBuilder.setActionButtonLabel(iosPayload.getAction());
            payloadBuilder.setLocalizedAlertMessage(iosPayload.getLocKey(), iosPayload.getLocArgs());
            payloadBuilder.setLocalizedAlertTitle(iosPayload.getTitleLocKey(), iosPayload.getTitleLocArgs());
            payloadBuilder.setLocalizedActionButtonKey(iosPayload.getActionLocKey());
            payloadBuilder.setLaunchImageFileName(iosPayload.getLaunchImage());
        }
        if (messageBody.getIosSound() != null) {
            final IOSSound iosSound = messageBody.getIosSound();
            payloadBuilder.setSound(iosSound.getName(), iosSound.getCritical(), iosSound.getVolume());
        }

        Map<String, Object> extras = messageBody.getExtras();
        if (extras != null && !extras.isEmpty()) {
            for (Map.Entry<String, Object> entry : extras.entrySet()) {
                payloadBuilder.addCustomProperty(entry.getKey(), entry.getValue());
            }
        }
        return payloadBuilder.build();
    }

    public void setIosBundle(String iosBundle) {
        this.iosBundle = iosBundle;
    }

}
