/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.service.fcm;

import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.auth.oauth2.AccessToken;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.messaging.Message;
import com.notifier.boot.push.server.service.fcm.exception.FirebaseMissingTokenException;
import com.notifier.boot.push.server.service.fcm.model.FcmSuccessResponse;
import io.netty.channel.ChannelOption;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.util.StringUtils;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.netty.transport.ProxyProvider;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.util.Arrays;
import java.util.Collections;

@Slf4j
public class FirebaseClient {

    private GoogleCredentials googleCredentials;

    private final WebClient webClient;

    private final FirebaseConfig config;

    private FirebaseClient(FirebaseConfig config) {
        this.config = config;
        this.webClient = WebClient.builder().clientConnector(new ReactorClientHttpConnector(buildHttpClient()))
                .filters(exchangeFilterFunctions -> exchangeFilterFunctions.addAll(Arrays.asList(logRequest(), logResponse())))
                .build();
    }

    private HttpClient buildHttpClient() {
        return HttpClient.create()
                .tcpConfiguration(tcpClient -> {
                    tcpClient = tcpClient.option(
                            ChannelOption.CONNECT_TIMEOUT_MILLIS,
                            config.getConnectionTimeout());
                    if (config.getProxy() != null) {
                        tcpClient = tcpClient.proxy(proxySpec -> {
                            ProxyProvider.Builder builder = proxySpec
                                    .type(ProxyProvider.Proxy.HTTP)
                                    .host(config.getProxy().getHost())
                                    .port(config.getProxy().getPort());
                            if (StringUtils.hasText(config.getProxy().getUsername())) {
                                builder.username(config.getProxy().getUsername());
                                builder.password(s -> config.getProxy().getPassword());
                            }
                            builder.build();
                        });
                    }
                    return tcpClient;
                });
    }

    public void start() {
        try {
            HttpTransport httpTransport;
            if (config.getProxy() != null) {
                Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(config.getProxy().getHost(), config.getProxy().getPort()));
                httpTransport = new NetHttpTransport.Builder().setProxy(proxy).build();
                if (StringUtils.hasText(config.getProxy().getUsername()) && StringUtils.hasText(config.getProxy().getPassword())) {
                    setProxyAuthentication();
                }
            } else {
                httpTransport = new NetHttpTransport.Builder().build();
            }
            googleCredentials = GoogleCredentials.fromStream(config.getPrivateKey(), () -> httpTransport)
                    .createScoped(Collections.singletonList("https://www.googleapis.com/auth/firebase.messaging"));
            googleCredentials.refresh();
        } catch (IOException e) {
            log.error("Error initialize firebase client", e);
        }
    }

    /**
     * Set proxy authentication for FCM.
     */
    private void setProxyAuthentication() {
        Authenticator.setDefault(new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                if (getRequestorType() == RequestorType.PROXY) {
                    if (getRequestingHost().equals(config.getProxy().getHost()) && getRequestingPort() == config.getProxy().getPort()) {
                        return new PasswordAuthentication(config.getProxy().getUsername(), config.getProxy().getPassword().toCharArray());
                    }
                }
                return null;
            }
        });
    }

    private AccessToken getAccessToken() throws FirebaseMissingTokenException {
        if (googleCredentials == null) {
            // In case FCM registration failed, access token is not available.
            // Exception is not thrown to allow test execution.
            return null;
        }
        try {
            googleCredentials.refreshIfExpired();
            return googleCredentials.getAccessToken();
        } catch (IOException e) {
            log.error("Error getting firebase access token", e);
            throw new FirebaseMissingTokenException("Error occured while refreshing FCM access token: " + e.getMessage(), e);
        }
    }

    private ExchangeFilterFunction logRequest() {
        return (clientRequest, next) -> {
            log.info("Request: {} {}", clientRequest.method(), clientRequest.url());
            clientRequest.headers()
                    .forEach((name, values) -> values.forEach(value -> log.info("{}={}", name, value)));
            return next.exchange(clientRequest);
        };
    }

    private ExchangeFilterFunction logResponse() {
        return ExchangeFilterFunction.ofResponseProcessor(clientResponse -> {
            log.info("Response: {}", clientResponse.headers().asHttpHeaders().get("property-header"));
            return Mono.just(clientResponse);
        });
    }

    public Mono<FcmSuccessResponse> exchange(Message message, boolean validationOnly) {
        if (!StringUtils.hasText(config.getProjectId())) {
            log.error("Push message delivery failed because FCM project ID is not configured.");
            return Mono.empty();
        }
        if (!StringUtils.hasText(config.getFirebaseMessageUrl())) {
            log.error("Push message delivery failed because FCM send message URL is not configured.");
            return Mono.empty();
        }

        Flux<DataBuffer> body = FcmModelConverter.convertMessageToFlux(message, validationOnly);

        WebClient.RequestBodySpec requestBody = webClient.post().uri(config.getFirebaseMessageUrl());

        AccessToken accessToken = null;
        try {
            accessToken = getAccessToken();
        } catch (FirebaseMissingTokenException e) {
            return Mono.error(new FirebaseMissingTokenException("Error occur while refreshing FCM access token"));
        }
        requestBody.header("Authorization", "Bearer " + accessToken.getTokenValue());

        return requestBody.contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromDataBuffers(body))
                .retrieve()
                .bodyToMono(FcmSuccessResponse.class)
                .onErrorResume(throwable -> throwable instanceof WebClientResponseException, Mono::error)
                .doOnNext((response) -> log.debug("Firebase result response :{}", response))
                .doOnError((cause) -> log.error("Error sending firebase message :{}", cause.getMessage(), cause));
    }

    public static final class Builder {

        private final FirebaseConfig.Builder firebaseConfigBuilder = new FirebaseConfig.Builder();

        public Builder proxy(FirebaseConfig.Proxy proxy) {
            firebaseConfigBuilder.proxy(proxy);
            return this;
        }

        public Builder credentials(InputStream credentialsStream) {
            firebaseConfigBuilder.privateKey(credentialsStream);
            return this;
        }

        public Builder credentials(byte[] credentials) {
            firebaseConfigBuilder.privateKey(new ByteArrayInputStream(credentials));
            return this;
        }

        public Builder connectionTimeout(int connectionTimeout) {
            firebaseConfigBuilder.connectionTimeout(connectionTimeout);
            return this;
        }

        public Builder projectId(String projectId) {
            firebaseConfigBuilder.projectId(projectId);
            return this;
        }

        public Builder firebaseMessageUrl(String messageUrl) {
            firebaseConfigBuilder.firebaseMessageUrl(messageUrl);
            return this;
        }

        public FirebaseClient build() {
            return new FirebaseClient(firebaseConfigBuilder.build());
        }

    }
}
