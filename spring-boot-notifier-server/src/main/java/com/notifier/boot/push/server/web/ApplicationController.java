/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.web;

import com.notifier.boot.push.server.domain.entity.ApplicationStatistic;
import com.notifier.boot.push.server.domain.repository.StatisticRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import reactor.core.publisher.Mono;

@Slf4j
@PushController
@ResponseBody
public class ApplicationController {

    private final StatisticRepository statisticRepository;

    public ApplicationController(StatisticRepository statisticRepository) {
        this.statisticRepository = statisticRepository;
    }

    /**
     * {@code GET /stat} : Get application statistic.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and body {@link ApplicationStatistic}
     */
    @GetMapping(path = "/stat", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<ApplicationStatistic>> getAppStatistic() {
        log.debug("REST request to get server statistic");
        return Mono.zip(statisticRepository.getTotalCount(),
                statisticRepository.getIosSuccess(),
                statisticRepository.getIosFailed(),
                statisticRepository.getAndroidSuccess(),
                statisticRepository.getAndroidFailed())
                .map(stat -> ApplicationStatistic.builder()
                        .total(stat.getT1())
                        .iosSuccess(stat.getT2())
                        .iosFailed(stat.getT3())
                        .androidSuccess(stat.getT4())
                        .androidFailed(stat.getT5())
                        .build()).map(ResponseEntity::ok);
    }

    /**
     * {@code POST /reset} : Reset server statistic.
     *
     * @return {@link ResponseEntity} with status {@code 200 (OK)}.
     */
    @PostMapping(path = "/stat/reset")
    public Mono<ResponseEntity<Void>> resetStatistic() {
        return statisticRepository.reset().map((aVoid) -> ResponseEntity.ok().build());
    }

}
