/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.web;

import com.notifier.boot.push.server.domain.entity.PushMessage;
import com.notifier.boot.push.server.domain.entity.PushSendingResult;
import com.notifier.boot.push.server.service.PushNotificationSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import reactor.core.publisher.Mono;

@Slf4j
@PushController
@ResponseBody
public class NotificationsController {

    private final PushNotificationSender notificationService;

    public NotificationsController(PushNotificationSender notificationService) {
        this.notificationService = notificationService;
    }

    /**
     * {@code POST /send} : Send push notifications to devices.
     *
     * @param pushMessage the {@link PushMessage} push message body.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and body {@link }
     */
    @PostMapping(path = "/push/send", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<PushSendingResult>> notifyPush(@RequestBody PushMessage pushMessage) {
        log.debug("REST request to send push notification :{}", pushMessage);
        return notificationService.sendNotifications(pushMessage).map(ResponseEntity::ok);
    }
}
