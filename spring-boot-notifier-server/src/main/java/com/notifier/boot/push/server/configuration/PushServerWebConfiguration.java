/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.configuration;

import com.notifier.boot.push.server.domain.repository.StatisticRepository;
import com.notifier.boot.push.server.service.PushNotificationSender;
import com.notifier.boot.push.server.util.jackson.PusherModule;
import com.notifier.boot.push.server.web.ApplicationController;
import com.notifier.boot.push.server.web.NotificationsController;
import com.notifier.boot.push.server.web.mapping.PushControllerReactiveHandlerMapping;
import com.notifier.boot.push.server.web.mapping.PushControllerServletHandlerMapping;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.reactive.accept.RequestedContentTypeResolver;

@Slf4j
@Configuration(proxyBeanMethods = false)
public class PushServerWebConfiguration {

    @Bean
    public PusherModule pusherModule() {
        return new PusherModule();
    }

    @Bean
    @ConditionalOnBean(PushNotificationSender.class)
    @ConditionalOnMissingBean
    public NotificationsController notificationsController(PushNotificationSender cloudSender) {
        return new NotificationsController(cloudSender);
    }

    @Bean
    @ConditionalOnMissingBean
    public ApplicationController applicationController(StatisticRepository statisticRepository) {
        return new ApplicationController(statisticRepository);
    }

    @Configuration(proxyBeanMethods = false)
    @ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.REACTIVE)
    public static class ReactiveApiMappingConfiguration {

        private final PushServerProperties properties;

        public ReactiveApiMappingConfiguration(PushServerProperties properties) {
            this.properties = properties;
        }

        @Bean
        public org.springframework.web.reactive.result.method.annotation.RequestMappingHandlerMapping pushHandlerMapping(
                RequestedContentTypeResolver webFluxContentTypeResolver) {
            org.springframework.web.reactive.result.method.annotation.RequestMappingHandlerMapping mapping = new PushControllerReactiveHandlerMapping(properties.getPath());
            mapping.setOrder(0);
            mapping.setContentTypeResolver(webFluxContentTypeResolver);
            return mapping;
        }
    }

    @Configuration(proxyBeanMethods = false)
    @ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
    @AutoConfigureAfter(WebMvcAutoConfiguration.class)
    public static class ServletApiMappingConfiguration {

        private final PushServerProperties properties;

        public ServletApiMappingConfiguration(PushServerProperties properties) {
            this.properties = properties;
        }

        @Bean
        public org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping pushHandlerMapping(ContentNegotiationManager contentNegotiationManager) {
            org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping mapping = new PushControllerServletHandlerMapping(properties.getPath());
            mapping.setOrder(0);
            mapping.setContentNegotiationManager(contentNegotiationManager);
            return mapping;
        }
    }
}
