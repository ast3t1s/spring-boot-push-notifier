/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.domain.entity;

import com.notifier.boot.push.server.domain.entity.enumaration.Status;
import lombok.Data;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Data
public class StatusInfo implements Serializable {

    private static final StatusInfo EMPTY = new StatusInfo(Collections.emptyMap());

    private final Map<Device, Status> statusMap;

    private StatusInfo(Map<Device, Status> statusInfoMap) {
        Assert.notNull(statusInfoMap, "'statusInfoMap' must not be null");
        if (statusInfoMap.isEmpty()) {
            this.statusMap = Collections.emptyMap();
        } else {
            this.statusMap = Collections.unmodifiableMap(statusInfoMap);
        }
    }

    public static StatusInfo empty() {
        return EMPTY;
    }

    public static StatusInfo fromDevice(Device device) {
        HashMap<Device, Status> statusMap = new HashMap<>();
        statusMap.put(device, Status.PENDING);
        return new StatusInfo(statusMap);
    }

    public static StatusInfo fromAndroid(@NonNull String token) {
        return fromDevice(Device.ofAndroid(token));
    }

    public static StatusInfo fromIos(@NonNull String token) {
        return fromDevice(Device.ofIos(token));
    }

    public StatusInfo append(StatusInfo statusInfo) {
        Map<Device, Status> newStatus = new LinkedHashMap<>(this.statusMap);
        newStatus.putAll(statusInfo.statusMap);
        return new StatusInfo(newStatus);
    }

    public StatusInfo withMessageStatus(Device device, Status status) {
        HashMap<Device, Status> newStatus = new HashMap<>();
        newStatus.put(device, status);
        return new StatusInfo(newStatus);
    }

    public StatusInfo ofPending(Device device) {
        return withMessageStatus(device, Status.PENDING);
    }

    public StatusInfo ofSent(Device device) {
        return withMessageStatus(device, Status.SENT);
    }

    public StatusInfo ofFailed(Device device) {
        return withMessageStatus(device, Status.FAILED);
    }

    public StatusInfo ofFailedDelete(Device device) {
        return withMessageStatus(device, Status.FAILED_DELETE);
    }

    public Optional<Status> get(Device device) {
        return Optional.ofNullable(this.statusMap.get(device));
    }

    public boolean isPresent(Device device) {
        return this.statusMap.containsKey(device);
    }

    public Map<Device, Status> getStatusMap() {
        return Collections.unmodifiableMap(this.statusMap);
    }

    public List<DeviceStatus> getStatuses() {
        return this.statusMap.entrySet().stream()
                .map(entry -> new DeviceStatus(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

    public Stream<DeviceStatus> stream() {
        return this.getStatuses().stream();
    }

    public boolean isSent(Device device) {
        return get(device).filter(status -> status == Status.SENT).isPresent();
    }

    public boolean isPending(Device device) {
        return get(device).filter(status -> status == Status.SENT).isPresent();
    }

    public boolean isFailed(Device device) {
        return get(device).filter(status -> status == Status.FAILED).isPresent();
    }

    public boolean isFailedDelete(Device device) {
        return get(device).filter(status -> status == Status.FAILED_DELETE).isPresent();
    }

    @Data
    public class DeviceStatus {

        private final Device device;

        private final Status status;
    }

}
