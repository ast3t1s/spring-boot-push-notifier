/*
 * Copyright (c) 2020 the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.notifier.boot.push.server.util;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;
import org.springframework.util.ResourceUtils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Base64;

@Slf4j
@UtilityClass
public class CertificateUtils {

    public InputStream loadCertificate(String resource) {
        if (ResourceUtils.isUrl(resource)) {
            log.debug("Load certificate from classpath");
            return loadCertificateFromClassPath(resource);
        }
        log.debug("Load certificate from base64");
        return loadCertificateFromBase64(resource);
    }

    public InputStream loadCertificateFromClassPath(String resource) {
        Assert.notNull(resource, "Resource location must not be null");
        try {
            return ResourceUtils.getURL(resource).openStream();
        } catch (Exception e) {
            log.error("Error load certificate from classpath", e);
            return new EmptyInputStream();
        }
    }

    public InputStream loadCertificateFromBase64(String base64Resource) {
        Assert.notNull(base64Resource, "Resource location must not be null");
        try {
            return new ByteArrayInputStream(Base64.getDecoder().decode(base64Resource));
        } catch (Exception e) {
            log.error("Error decoding certificate from base64", e);
            return new EmptyInputStream();
        }
    }
}
