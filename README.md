Spring Boot Push Notifier
===============================
[![MIT License](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://opensource.org/licenses/MIT)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.notifier/spring-boot-push-notifier/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.notifier/spring-boot-push-notifier)
[![pipeline status](https://gitlab.com/ast3t1s/spring-boot-push-notifier/badges/master/pipeline.svg)](https://gitlab.com/ast3t1s/spring-boot-push-notifier/-/commits/master)
[![coverage report](https://gitlab.com/ast3t1s/spring-boot-push-notifier/badges/master/coverage.svg)](https://gitlab.com/ast3t1s/spring-boot-push-notifier/-/commits/master)
[![snapshot-version](https://img.shields.io/badge/snapshot-1.1.0-brightgreen.svg?style=flat-square&logo=appveyor)](https://gitlab.com/ast3t1s/spring-boot-push-notifier/-/commits/master)
[![last-commit](https://gitlab.com/ast3t1s/spring-boot-push-notifier/-/jobs/artifacts/master/raw/last-commit.svg?job=badge)](https://gitlab.com/ast3t1s/spring-boot-push-notifier/-/commits/master)

![](./images/logo.png)

Spring Boot Push Notifier is a general push notification library for spring boot applications. It proxies push requests to APNs and FCM and asynchronously
executes them. It helps you when you need to bulky sends push notifications to your users or when some other API server 
which must response quickly needs to push.

## Contents

- [Spring Boot Push Notifier](#spring-boot-push-notifier)
    - [Contents](#contents)
    - [Support Platform](#support-platform)
    - [Features](#features)
    - [Usage](#usage)
        - [Set up](#set-up)
        - [Configuration](#configuration)
    - [Web API](#web-api)
        - [GET /stat](#get-stat)
        - [POST /stat/reset](#post-statreset)
        - [POST /push/send](#post-pushsend)
        - [Request body](#request-body)
        - [iOS alert payload](#ios-alert-payload)
        - [iOS sound payload](#ios-sound-payload)
        - [Android notifications payload](#android-notification-payload)
    - [Build](#build)
    - [License](#license)    

## Support Platform

- [Apple APNs](https://developer.apple.com/library/content/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/APNSOverview.html)
- [Google FCM](https://firebase.google.com/docs/cloud-messaging/)  


## Features

- Support [Firebase Cloud Messaging](https://firebase.google.com/docs/cloud-messaging) using [Firebase Admin SDK](https://github.com/firebase/firebase-admin-java) library for Android.
- Support [HTTP/2](https://http2.github.io/) Apple Push Notification Service using [pushy](https://github.com/jchambers/pushy) library.
- Support Spring Boot Application Integration.
- Support Web API to send push notifications.
- Support /stat show notification success and failure counts.
- Support /stat/reset reset notification statistic.
- Support store app stat to memory, Redis or custom storage.
- Support p8, p12 or pem format of iOS certificate file.

## Usage

### Set up

First, you need to setup your server. To do this just setup a simple spring boot application (using start.spring.io/). As Spring Boot Push Notifier is capable of running as servlet or webflux application, you need to decide on this and add
the according Spring Boot Starter. In this example we are using the webflux web starter.

Add Spring Push Notifier to your dependencies:

```xml
<dependency>
	<groupId>com.notifier</groupId>
	<artifactId>spring-boot-notifier-server-starter</artifactId>
    <version>{project-version}</version>
</dependency>

```

Pull in the Spring Push Notifier configuration via adding @EnablePushNotificationServer to your configuration:
```java
@SpringBootApplication
@EnablePushNotificationServer
public class SpringBootPushNotifierSample {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootPushNotifierSample.class, args);
    }
}

```

### Configuration

```yaml
spring:
    push:
        statistic:
            redis:
              enabled: true
        firebase:
            enabled: true
            message-url: https://fcm.googleapis.com/v1/projects/%s/messages:send
            project-id: project-id
            credentials: classpath:firebase-credentials.json #or credentials in base64 format
            connection-timeout: 100000ms
            proxy-enabled: true
            proxy:
              host: https://fcm-proxy-host.com
              posrt: 5222
              username: fcm-proxy-username
              password: fcm-proxy-password
        apns:
            enabled: true
            development: true
            connection-timeout: 100000ms
            ios-key-id: "" # KeyID from developer account (Certificates, Identifiers & Profiles -> Keys)
            ios-team-id: "" # TeamID from developer account (View Account -> Membership)
            ios-bundle: "" # BundleID from developer account
            certificate: classpath:key.pem #or iOS key from base64 input
            proxy-enabled: true
            proxy:
              host: https://apns-proxy-host.com
              port: 5222
              username: apns-proxy-username
              password: apns-proxy-password

```

## Web API

Spring Boot Push Notifier support the following API.

- **GET**  `/stat` show notification success and failure counts.
- **POST** `/stat/reset` reset server notifications statistic.
- **POST** `/push/send` push ios and android notifications.


### GET /stat

Show success or failure counts information of notification.

```json
{
  "ios": {
    "success": 14,
    "failed": 15
  },
  "android": {
    "success": 31,
    "failed": 34
  },
  "total": 94
}
```

### POST /stat/reset

Reset statistic information of notification.

The response should be 200(OK) with empty body.

### POST /push/send

```json
{
  "title": "message title",
  "body": "message body",
  "topic": "test topic",
  "badge": 4,
  "category": "test - category",
  "sound": "sound",
  "collapse_key": "test-key",
  "icon": "my icon",
  "valid_until": "2020-06-22T14:09:01.398221100Z",
  "time_to_live": 500,
  "mutable_content": true,
  "ios_alert": {
    "title": "iOS alert title",
    "body": "iOS alert body",
    "subtitle": "iOS subtitle",
    "action": "iOS action",
    "action-log-arg": "PLAY",
    "launch-image": "iOS launch image",
    "loc-key" : "GAME_PLAY_REQUEST_FORMAT",
    "loc-args": [ "Jenna", "Frank"],
    "title-loc-key": "TILE REQUEST",
    "title-loc-args": ["Title1", "Title2"]
  },
  "ios_sound" : {
    "critical": true,
    "name": "default",
    "volume": 2.0
  },
  "android_payload" : {
    "icon": "some-icon",
    "tag": "some tag",
    "color": "#rrggbb",
    "click_action": "click notification",
    "body_loc_key": "loc key",
    "body_loc_args": ["arg1", "arg2"],
    "title_loc_key": "title loc key",
    "title_loc_args": ["title1", "title2"]
  },
  "extras": {
    "data": "some-data",
    "field": "some value"
  },
  "tokens": [
    {
      "token": "token1",
      "platform": "ios"
    },
    {
      "token": "token2",
      "platform": "android"
    }
  ]
}
```

Success response:


```json
{
  "ios": {
    "pending": 0,
    "success": 0,
    "failed": 0
  },
  "android": {
    "pending": 0,
    "success": 5,
    "failed": 5
  },
  "total": 10,
  "unregistered_tokens": [
    {
      "platform": "ios",
      "token": "token1"
    },
    {
      "platform": "android",
      "token": "token2"
    }
  ]
}
```

Error response message table:

| status code | message                                          |
|-------------|--------------------------------------------------|
| 400         | Notifications body is empty                      |
| 400         | Device token or platform is empty or incorrect   |
| 500         | Internal Server Error                            |


### Request body

The following is a parameter table for each notification.

| name                    | type         | description                                                                                       | required | note                                                          |
|-------------------------|--------------|---------------------------------------------------------------------------------------------------|----------|---------------------------------------------------------------|
| title                   | string       | notification title                                                                                | -        |                                                               |
| message                 | string       | message for notification                                                                          | -        |                                                               |
| topic                   | string       | topic for notification                                                                            | -        |
| badge                   | int          | notifications badge count
| icon                    | string       | notification sound
| sound                   | string       | notification sound
| collapse_key            | string       | An identifier you use to coalesce multiple notifications into a single notification for the user
| icon                    | string       | notification icon
| valid_until             | string       | 
| time_to_live            | long         | This parameter specifies how long (in seconds) the message should be kept in FCM storage if the device is offline. The maximum time to live supported is 4 weeks, and the default value is 4 weeks.
| category                | string       

### Device tokens
| name        | type         | description                              | required | note                             |
|-------------|--------------|------------------------------------------|----------|----------------------------------|
| token       | string       | device token                             | +        |                                  |
| platform    | string       | device platform (iOS, Android)           | +        | value must be ios or android     |

### iOS alert payload

| name           | type             | description                                                                                      | required | note |
|----------------|------------------|--------------------------------------------------------------------------------------------------|----------|------|
| title          | string           | Apple Watch & Safari display this string as part of the notification interface.                  | -        |      |
| body           | string           | The text of the alert message.                  | -        |      |
| subtitle       | string           | Apple Watch & Safari display this string as part of the notification interface.                  | -        |      |
| action         | string           | The label of the action button. This one is required for Safari Push Notifications.              | -        |      |
| action-loc-key | string           | If a string is specified, the system displays an alert that includes the Close and View buttons. | -        |      |
| launch-image   | string           | The filename of an image file in the app bundle, with or without the filename extension.         | -        |      |
| loc-args       | array of strings | Variable string values to appear in place of the format specifiers in loc-key.                   | -        |      |
| loc-key        | string           | A key to an alert-message string in a Localizable.strings file for the current localization.     | -        |      |
| title-loc-args | array of strings | Variable string values to appear in place of the format specifiers in title-loc-key.             | -        |      |
| title-loc-key  | string           | The key to a title string in the Localizable.strings file for the current localization.          | -        |      |

See more detail about [APNs Remote Notification Payload](https://developer.apple.com/library/content/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/PayloadKeyReference.html).

request format:

```json
{
  "ios_alert" : {
      "title": "iOS alert title",
      "body": "iOS alert body",
      "subtitle": "iOS subtitle",
      "action": "iOS action",
      "action-log-arg": "PLAY",
      "launch-image": "iOS launch image",
      "loc-key" : "GAME_PLAY_REQUEST_FORMAT",
      "loc-args": [ "Jenna", "Frank"],
      "title-loc-key": "TILE REQUEST",
      "title-loc-args": ["Title1", "Title2"]
    }
}
```

### iOS sound payload

| name           | type             | description                                                                                      | required | note |
|----------------|------------------|--------------------------------------------------------------------------------------------------|----------|------|
| name           | string           | sets the name value on the aps sound dictionary.                                                 | -        |      |
| volume         | float32          | sets the volume value on the aps sound dictionary.                                               | -        |      |
| critical       | boolean          | sets the critical value on the aps sound dictionary.       

request format:

```json
{
  "sound": {
    "critical": true,
    "name": "default",
    "volume": 2.0
  }
}
```

### Android notification payload

| name           | type   | description                                                                                               | required | note |
|----------------|--------|-----------------------------------------------------------------------------------------------------------|----------|------|
| icon           | string | Indicates notification icon.                                                                              | -        |      |
| tag            | string | Indicates whether each notification message results in a new entry on the notification center on Android. | -        |      |
| color          | string | Indicates color of the icon, expressed in #rrggbb format                                                  | -        |      |
| click_action   | string | The action associated with a user click on the notification.                                              | -        |      |
| body_loc_key   | string | Indicates the key to the body string for localization.                                                    | -        |      |
| body_loc_args  | string | Indicates the string value to replace format specifiers in body string for localization.                  | -        |      |
| title_loc_key  | string | Indicates the key to the title string for localization.                                                   | -        |      |
| title_loc_args | string | Indicates the string value to replace format specifiers in title string for localization.                 | -        |      |

See more details about [Firebase Cloud Messaging HTTP Protocol reference](https://firebase.google.com/docs/cloud-messaging/http-server-ref#send-downstream)

request format:

```json
{
  "android_payload" : {
      "icon": "some-icon",
      "tag": "some tag",
      "color": "#rrggbb",
      "click_action": "click notification",
      "body_loc_key": "loc key",
      "body_loc_args": ["arg1", "arg2"],
      "title_loc_key": "title loc key",
      "title_loc_args": ["title1", "title2"]
    }

}
```

## Build
**Requirements:**
* JDK >= 1.8

Please make sure you set `$JAVA_HOME` points to the correct JDK on your `$PATH`.

```shell
./mvnw clean package
```

## License

Copyright 2020.

Licensed under the MIT License.